<?php
return array(
      'Api\Model\CronModel'       		=> __DIR__ . '/src/Api/Model/CronModel.php',
      'Api\Controller\IndexController' 	=> __DIR__ . '/src/Api/Controller/IndexController.php',
	  'Api\Language\Lingo' 				=> __DIR__ . '/src/Api/Language/Lingo.php',
	  'Api\Language\de_DE' 				=> __DIR__ . '/src/Api/Language/de_DE.php',
	  'Api\Language\en_AU' 				=> __DIR__ . '/src/Api/Language/en_AU.php',
      'Api\Module'                     	=> __DIR__ . '/Module.php',  
);