<?php
return array(
		
    'router' => array(
        'routes' => array(

            'server' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/server',
                    'defaults' => array(
                        'controller'    => 'Server\Controller\Index',
                        'action'        => 'index',
                    ),
                ),	
           ),	
        	'server-register' => array(
        			'type'    => 'Literal',
        			'options' => array(
        					'route'    => '/server/register/',
        					'defaults' => array(
        							'controller'    => 'Server\Controller\Register',
        							'action'        => 'index',
        					),
        			),
        			'may_terminate' => true,
        			'child_routes' => array(
        					'activate' => array(
        							'type' => 'Segment',
        							'options' => array(
        									'route'    => 'activate[/:key]',
        									'defaults' => array(
        											'controller' => 'Server\Controller\Register',
        											'action'     => 'index',
        									),
        							),
        					),
        			),

        	),

        	'server-account' => array(
        			'type'    => 'Literal',
        			'options' => array(
        					'route'    => '/server/account/',
        					'defaults' => array(
        							'controller'    => 'Server\Controller\Account',
        							'action'        => 'index',
        					),
        			),
        			'may_terminate' => true,
        			'child_routes' => array(
        					'options' => array(
        							'type' => 'Segment',
        							'options' => array(
        									'route'    => 'option/',
        									'defaults' => array(
        											'controller' => 'Server\Controller\Account',
        											'action'     => 'option'
        									),
        							),
        					),
        					'do' => array(
        							'type' => 'Segment',
        							'options' => array(
        									'route'    => 'do[/:do]',
        									'defaults' => array(
        											'controller' => 'Server\Controller\Account',
        											'action'     => 'index'
        									),
        							),
        					),
        					'remove' => array(
        							'type' => 'Segment',
        							'options' => array(
        									'route'    => 'do[/:do]/remove[/:remove]',
        									'defaults' => array(
        											'controller' => 'Server\Controller\Account',
        											'action'     => 'index'
        									),
        							),
        					),
        					'master' => array(
        							'type' => 'Segment',
        							'options' => array(
        									'route'    => 'do[/:do]/master[/:master]',
        									'defaults' => array(
        											'controller' => 'Server\Controller\Account',
        											'action'     => 'index'
        									),
        							),
        					),
        					'player' => array(
        							'type' => 'Segment',
        							'options' => array(
        									'route'    => 'do[/:do]/player[/:player]',
        									'defaults' => array(
        											'controller' => 'Server\Controller\Account',
        											'action'     => 'index'
        									),
        							),
        					),
        			),

        	),
        		
			'server-mator' => array(
        			'type'    => 'Literal',
        			'options' => array(
        					'route'    => '/server/mator/',
        					'defaults' => array(
        							'controller'    => 'Server\Controller\Mator',
        							'action'        => 'index',
        					),
        			),
        			'may_terminate' => true,
        			'child_routes' => array(
        					'dash' => array(
        							'type' => 'Segment',
        							'options' => array(
        									'route'    => 'dash/',
        									'defaults' => array(
        											'controller' => 'Server\Controller\Mator',
        											'action'     => 'dash',
        									),
        							),
        					),
        			),

        	),

        ),
    ),
		
    'controllers' => array(
        'invokables' => array(
            'Server\Controller\Index' 		=> 'Server\Controller\IndexController',
            'Server\Controller\Register' 	=> 'Server\Controller\RegisterController',
            'Server\Controller\Account' 	=> 'Server\Controller\AccountController',
        	'Server\Controller\Mator' 	=> 'Server\Controller\MatorController'
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           	=> __DIR__ . '/../../../public/templates/classic/index.phtml',
            'server/index/index' 		=> __DIR__ . '/../view/server/index/index.phtml',
    		'server/register/index' 	=> __DIR__ . '/../view/server/register/index.phtml',
            'server/error/404'         	=> __DIR__ . '/../view/server/error/404.phtml',
            'server/error/index'        => __DIR__ . '/../view/server/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);


