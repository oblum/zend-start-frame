<?php
class assigner_Plugin extends Zend_Controller_Plugin_Abstract 
{
	
	public function preDispatch(Zend_Controller_Request_Abstract $request)
	{
		$layout = Zend_Layout::getMvcInstance();
		$view = $layout->getView();
		$view->whatever = 'foo';
	}
	

	public function setVar($name, $value)
	{
		$layout = Zend_Layout::getMvcInstance();
		$view = $layout->getView();
		$view->$name = $value;
		
	}
	
}
