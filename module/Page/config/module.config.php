<?php
return array (
    'router' => array (
        'routes' => array (
            'page' => array (
                'type'    => 'Literal',
                'options' => array (
                    'route'    => '/page',
                    'defaults' => array (
                        'controller'    => 'Page\Controller\Index',
                        'action'        => 'index',
                     ),
                    
                    'show' => array (
                        'type' => 'Segment',
                        'options' => array (
                            'route'    => 'show[/:show]',
                            'defaults' => array (
                                'controller' => 'Page\Controller\Index',
                                'action'     => 'index'
                            ),
                        ),
                    ),
                ),	
            ),	
        ),
    ),
		
    'controllers' => array(
        'invokables' => array(
            'Page\Controller\Index' 		=> 'Page\Controller\IndexController',
        ),
    ),
    
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'page/index/index' 		=> __DIR__ . '/../view/page/index/index.phtml',
            'page/error/404'        => __DIR__ . '/../view/page/error/404.phtml',
            'page/error/index'      => __DIR__ . '/../view/page/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);


