<?php

namespace Page\Controller;

use Zend\Mvc\Controller\AbstractActionController, 
    Zend\View\Model\ViewModel, 
    Zend\Session\Container, 
    Zend\InputFilter\InputFilter, 
    Zend\InputFilter\Input, 
    Zend\Validator, 
    Page\Model\feedTattsModel, 
    Page\Language\Lingo;

class IndexController extends AbstractActionController
{

  /**
   * @desc Template Vars
   * @type array
   */
  private $tplVars = array();

  /**
   * @desc Error Messages
   * @type array
   */
  public $error = array();

  /**
   *@desc Helper Object
   * @var object
   */
  public $helperAdapter;

  /**
   *@desc Db table
   * @var object
   */
  protected $pageModel;
  
  public $tattsModel;


/** ------------------------------------- Action controller -----------------*/

  /**
   * @des start of process
   */
  public function indexAction()
  {
    // @global controller set up
    $this->setEnv();
    $this->mergeErrors();
    $this->defineTplVars();
	$this->getTattsModel();
	
	$sports = $this->tattsModel->getSports();
    
    $this->tplVars['data'] = array('I ', 'am ', 'working ', 'fine. ');
	
	$this->tplVars['sports'] = $sports;

    $view = new ViewModel($this->tplVars);
    $view->setTemplate('page/index/index');

    return $view;
  }


  /** ---------------------------- Controller set up ---------------------*/

  
   /**
   * @desc define template vars to start
   */
  private function defineTplVars()
  {
    $this->tplVars['error'] = $this->error;
    $this->tplVars['data'] = '';
    $this->tplVars['lingo'] = $this->Lingo->String;

    return false;
  }
  
  /**
   * @desc set up dependencies
   */
  private function setEnv()
  {
    // get helper
    $this->getHelper();
    // get model
    $this->getModel();
    // get language
    $this->getLingo('de_DE');
    // set lingo in model
    $this->pageModel->setLingo($this->Lingo);

    $this->layout = $this->layout();
    $this->layout->lang = $this->Lingo->String;
  }


  /**
   * @desc merge the errors for printing
   */
  private function mergeErrors()
  {
    $this->error = array_merge($this->error, $this->pageModel->getError());
    $this->tplVars['error'] = $this->error;
    return false;
  }


  public function getModel()
  {
    if (!$this->pageModel) {
      $sm = $this->getServiceLocator();
      $this->pageModel = $sm->get('page-model');
    }
    return false;
  }
  
  
  public function getTattsModel()
  {
    if (!$this->tattsModel) {
      $sm = $this->getServiceLocator();
      $this->tattsModel = $sm->get('tatts-model');
    }
    return false;
  }


  public function getLingo($lang)
  {
    if (!isset($this->Lingo)) {
      $this->Lingo = new Lingo($lang);
    }
    return false;
  }


  public function getHelper()
  {

    if (!$this->helperAdapter) {
      $sm = $this->getServiceLocator();
      $this->helperAdapter = $sm->get('helper-adapter');
    }
    return false;
  }

}
