<?php
namespace Page\Model;

use Zend\Db\Adapter\Adapter,
    Zend\Db\Sql\Sql,
    Zend\Db\Sql\Select,
    Zend\Db\Sql\Insert,
    Zend\Db\ResultSet\ResultSet,
    Zend\Mail\Message,
    Zend\Mime\Message as MimeMessage,
    Zend\Mime\Part as MimePart,
    Zend\Db\TableGateway\TableGateway,
    Zend\Mail\Transport\Smtp as SmtpTransport,
    Zend\Mail\Transport\SmtpOptions,
    Zend\Crypt\Password\Bcrypt,
	Application\Helper\HelperModel as SuperModel;
    

class TattsModel extends SuperModel
{

    /**
     * @desc Helper class
     * @var object
     */
    protected $gHelper;
    
    /**
     * @desc Set errors
     * @var array
     */
    public $error = array();
    
    /**
     * @desc Set errors
     * @var array
     */
    public $msg = array();
    
	/**
	 * @desc Set environment, get injections, set stuff
	 * @param Adapter $adapter
	 * @param Helper $gHelper
	 */
    public function __construct(Adapter $adapter, $gHelper)
    {  
    	parent::__construct($adapter);
    	$this->dbAdapter	= $adapter;
    	$this->gHelper		= $gHelper;
		
    }
	
	/**
	 * @desc public function get the sport feed
	 */
	public function getSports()
	{
		$this->getAllSports();
		return $this->dataSports;
	}
	
	/**
	 * get/set all sports feed
	 * i must be able to set the feed in the class for later use and also to return it using the other class
	 */
	private function getAllSports()
	{	
		$feedUrl = 'https://tatts.com/pagedata/sports/sports.xml';
		$this->dataSports = $this->getDataStream($feedUrl);
		$this->saveSports();
	}
	
	/**
    * @desc get the data stream via curl
    * @param string $url
    * @return mixed
    */
	private function getDataStream ($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_FAILONERROR,1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		$data = curl_exec($ch);  
		
		if(curl_errno($ch)){
			echo 'error:' . curl_error($ch);
		}
		
		curl_close($ch);
		   
		// create xml object
		$xmlData = simplexml_load_string($data);
		// create an array to make it easier to get to the data
		$xmlData = json_decode(json_encode((array)$xmlData), 1);
		
		// return result array
		if(is_array($xmlData) && count($xmlData)){
			return $xmlData;
		}
		   
		return false;
	
	}
	
	/**
	 * @desc update the sport list
	 */
	private function saveSports()
	{
		$sportid = 0;
		
		if(is_array($this->dataSports) && count($this->dataSports)){
			
			foreach($this->dataSports['Sport'] AS $key => $value){
				
				// make sure the sport doesn't exists before insert
				$check = "SELECT 
							count(`sportid`) as counter, `sportid` 
								FROM `aff_sports`
							WHERE
								`tatts_sportsid` = ".(int)$value['@attributes']['SportId']." 
				";				
				
				$statement = $this->dbAdapter->query($check);
			    $result = $statement->execute(array());
			    $result = $this->gHelper->formResult($result);
				
				// new sport, yes, enter it
				if(isset($result[0]['counter']) && $result[0]['counter'] < 1) {
					
					$query = "INSERT INTO `aff_sports`
								(
								`name`, 
								`shortname`,
								`tatts_sportsid`
								)
								VALUES
								(
								'".$value['@attributes']['SportName']."',
								'".$value['@attributes']['SportNameShort']."',
								".(int)$value['@attributes']['SportId']."
								)
					";
				
					$this->dbAdapter->query($query)
						->execute(array());
						// need the sport id
					$sportid = $this->dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
				}else{
					// get sportid
					$sportid = $result[0]['sportid'];
				}
				
				// insert leagues
				if(isset($value['League']) && count($value['League'])){
					
					$this->saveLeagues($value['League'], $value['@attributes']['SportId'], $sportid);
				}
			}
		}
		
		
	    $this -> setError('test');
		die('ddss');
	    return false;
			
	}

	/**
	 * @save leagues 
	 */
	private function saveLeagues($leagues, $tattsid, $sportsid)
	{
		$insertString = NULL;
		
		
		foreach($leagues AS $value){

			// make sure the sport doesn't exists before insert
			$check = "SELECT 
						count(`id`) as counter
							FROM `aff_leagues`
						WHERE
							`tatts_sportsid` = ".(int)$tattsid." 
			";				
			
			$statement 	= $this->dbAdapter->query($check);
		    $result 	= $statement->execute(array());
		    $result 	= $this->gHelper->formResult($result);
			

			// new sport, yes, enter it
			if(isset($result[0]['counter']) && $result[0]['counter'] < 1) {

				if(isset($value['@attributes']) && array_key_exists('LeagueId',$value['@attributes'])) {
					
					//print '<pre>';
					//print_r($value['@attributes']);
					//print '</pre>';
					
					$query = "(
								".(int)$value['@attributes']['LeagueId'].",
								".(int)$sportsid.",
								".(int)$tattsid.",
								'".$value['@attributes']['LeagueName']."'
								),";
					
					$insertString .= $query;
				}
			}
		}

		if(!empty($insertString)){
			$string = substr($insertString, 0, -1);
			$string = "INSERT INTO `aff_leagues` (`leagueid`,`sportid`, `tatts_sportsid`,`name`) VALUES ".$string;
		
			try {
				$this->dbAdapter->query($string)
					->execute(array());
				} catch (\Zend\Db\Exception $e) {
					print $e;
			}
		}
	}
	
}
