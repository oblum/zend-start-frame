<?php
namespace Api\Helper;

use Zend\Db\Adapter\Adapter, 
    Zend\Db\Sql\Sql, 
    Zend\Db\Sql\Select, 
    Zend\Db\Sql\Insert, 
    Zend\Db\ResultSet\ResultSet, 
    Zend\Mail\Message, 
    Zend\Mime\Message as MimeMessage, 
    Zend\Mime\Part as MimePart, 
    Zend\Db\TableGateway\TableGateway, 
    Zend\Mail\Transport\Smtp as SmtpTransport, 
    Zend\Mail\Transport\SmtpOptions;

class HelperModel 
{

  protected $dbAdapter;

  public function __construct(Adapter $adapter) 
  {
    $this->dbAdapter = $adapter;
  }

  /**
   *@desc Do a single insert
   * @param string $table
   * @param array $values
   * @return boolean
   */
  protected function processDbInsert($table, $values) 
  {

    $insert = new Insert($table);
    $insert->values($values);
    $statment = $this->dbAdapter->createStatement();
    $insert->prepareStatement($this->dbAdapter, $statment);

    $statment->execute();

  }

  /**
   * @desc form a niver array to work with
   * @param array $results
   */
  protected function formResult($results) 
  {
    $result = array();

    foreach ($results as $key => $value) {
      $result[$key] = $value;
    }

    return $result;

  }

  /**
   * @desc set lingo
   * @param object $lingo
   */
  public function setLingo($lingo) 
  {
    $this->Lingo = $lingo;
  }

  /**
   * @desc Get errors calling from controller class
   */
  public function getError() 
  {
    return $this->error;
  }

  /**
   * @desc Set internal errors
   * @param string $error
   */
  protected function setError($error) 
  {
    $this->error[] = $error;
  }

  /**
   * @desc Get msg calling from controller class
   */
  public function getMsg() 
  {
    return $this->msg;
  }

  /**
   * @desc Set internal msg
   * @param string $error
   */
  protected function setMsg($msg) 
  {
    $this->msg[] = $msg;
  }

}
