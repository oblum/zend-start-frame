<?php

namespace Page;

use Page\Model\PageModel,
    Application\Helper\HelperAdapter as HelperAdapter,
    Zend\Mvc\ModuleRouteListener,
    Zend\Db\Adapter\Adapter as DbAdapter,
	  Application\Controller\HelperModel as SuperModel;

class Module
{
	
	public function onBootstrap($e)
    {

    	
    }
    
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array (
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array (
                'namespaces' => array (
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
     
    
    public function getServiceConfig()
    {
        return array (
            'factories' => array ( 
                'helper-adapter' => function($sm) {
                    $helperAdapter = new HelperAdapter();
                    return $helperAdapter;
                },
                'page-model' => function($sm) {
                    $config = $sm->get('config');
                    $config = $config['db'];
                    $pageModel = new Model\PageModel(new DbAdapter($config), new HelperAdapter());
                    return $pageModel;
                },
                'tatts-model' => function($sm) {
                    $config = $sm->get('config');
                    $config = $config['db'];
                    $tattsModel = new Model\TattsModel(new DbAdapter($config), new HelperAdapter());
                    return $tattsModel;
                },
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}