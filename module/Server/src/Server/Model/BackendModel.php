<?php
namespace Server\Model;

use Zend\Db\Adapter\Adapter,
    Zend\Db\Sql\Sql,
    Zend\Db\Sql\Select,
    Zend\Db\Sql\Insert,
    Zend\Db\ResultSet\ResultSet,
    Zend\Mail\Message,
    Zend\Mime\Message as MimeMessage,
    Zend\Mime\Part as MimePart,
    Zend\Db\TableGateway\TableGateway,
    Zend\Mail\Transport\Smtp as SmtpTransport,
    Zend\Mail\Transport\SmtpOptions,
    Zend\Crypt\Password\Bcrypt;
    

class BackendModel 
{

    /**
     * @desc Helper class
     * @var object
     */
    protected $gHelper;
    
    /**
     *@desc db table users
     * @var array
     */
    public $tableUsers 		= array();
    
    /**
     * @desc db table for accounts
     * @var array
     */
    public $tableAccounts 	= array();
    
    /**
     * clear password
     * @var string
     */
    public $clearPassword	= NULL;
    
    /**
     * @desc Set errors
     * @var array
     */
    public $error = array();
    
    /**
     * @desc Set errors
     * @var array
     */
    public $msg = array();


	/**
	 * @desc Set environment, get injections, set stuff
	 * @param Adapter $adapter
	 * @param Helper $gHelper
	 */
    public function __construct(Adapter $adapter, $gHelper)
    {
    	parent::__construct($adapter);
    	
    	$this->dbAdapter	= $adapter;
    	$this->gHelper		= $gHelper;
    	
        // table users vars
        $this->setTableUsersVars();
        $this->setTableAccountVars();
        $this->setTableAssignerTable();
    }


    /**
     *@desc Do a single insert
     * @param string $table
     * @param array $values
     * @return boolean 
     */
    private function processDbInsert($table, $values)
    {
        $insert     = new Insert($table);
        $insert->values($values); 
        $statment   = $this->dbAdapter->createStatement();
        $insert->prepareStatement($this->dbAdapter, $statment);
        
        $statment->execute();

    }


  	private function setTableAssignerTable()
  	{
  		$this->tableAssigner = array (
      		'asicreated'	=> date('Y-m-d H:i:s'),
      		'asiacoid'		=> '',
  	    	'asiusrid'		=> '',
  	    	'asicodeused'	=> '',
      	);
  	}
	

    private function setTableAccountVars()
    {
    	$this->tableAccounts = array (
    		'acocode'		=> $this->getCode(12),
    		'aconame'		=> 'NoName',
	    	'acocreated'	=> date('Y-m-d H:i:s'),
	    	'acostatus'		=> '0',
    	);
    }
    
    
    private function setTableUsersVars()
    {
        $this->tableUsers = array(
            'usrname'           => '',
            'usremail'          => '',
            'usrgender'         => '',
            'usrbirthday'       => '',
            'usrdescription'    => '',
            'usrmedia'          => '',
            'usrcreated'        => '',
            'usrlastrefresh'    => '',
            'usrstatus'         => '',
            'usrpassword'       => '',
        	   'usractivate'      => ''
        );
    }
    
}
