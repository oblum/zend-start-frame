
<div id="dash-board">

	<?php 
	if($sysmsg){
		echo $sysmsg;
	} 
	?>
	
	<?php 
	if($errmsg){
		echo $errmsg;
	}
	?>
	
	
	<div id="search-user">
	
		<h2><?php echo $lingo['admSearchUser'];?></h2>
		
		<form action="/server/mator/" method="post" id="user-search">
		
			<div class="input-container">
				<input type="text" class="norm" name="search-user" value="<?php echo $lingo['admSearch'];?>" onfocus="if (this.value=='<?php echo $lingo['admSearch'];?>') this.value = ''" onblur="if (this.value == '') this.value = '<?php echo $lingo['admSearch'];?>'" />
				<input type="submit" name="submitter" value="<?php echo $lingo['admSearch'];?>" class="button medium" />
			</div>
			
		</form>
		<!-- end user-search -->
	
	</div>
	
	<div id="search-account">
	
		<h2><?php echo $lingo['admSearchAccount'];?></h2>
		
		<form action="/server/mator/" method="post" id="account-search">
		
			<div class="input-container">
				<input type="text" class="norm" name="search-account" value="<?php echo $lingo['admSearch'];?>" onfocus="if (this.value=='<?php echo $lingo['admSearch'];?>') this.value = ''" onblur="if (this.value == '') this.value = '<?php echo $lingo['admSearch'];?>'" />
				<input type="submit" name="submitter" value="<?php echo $lingo['admSearch'];?>" class="button medium" />
			</div>
			
		</form>
		<!-- end account-search -->
	
	</div>
		
	<div id="search-object">
	
		<h2><?php echo $lingo['defObject'].' '.$lingo['admObject'];?></h2>
		
		<form action="/server/mator/" method="post" id="object-search">
		
			<div class="input-container">
				<input type="text" class="norm" name="search-object" value="<?php echo $lingo['admSearch'];?>" onfocus="if (this.value=='<?php echo $lingo['admSearch'];?>') this.value = ''" onblur="if (this.value == '') this.value = '<?php echo $lingo['admSearch'];?>'" />
				<input type="submit" name="submitter" value="<?php echo $lingo['admSearch'];?>" class="button medium" />
			</div>
			
		</form>
		<!-- end account-search -->
	
	</div>
		
</div>
<!-- end dash-board -->
