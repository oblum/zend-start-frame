<?php

namespace Page\Language;

use Page\Language\de_DE, 
	Page\Language\en_AU;

class Lingo {
	
	public $String;
	
	public function __construct($lang){
		
		if($lang == 'de_DE'){
			$p = new de_DE();
		}elseif($lang == 'en_AU'){
			$p = new en_AU();
		}else{
			$p = new de_DE();
		}
		
		$this->String = $p->String;
			
	}
// -----------------
	
}
