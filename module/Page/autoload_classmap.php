<?php
return array(
      'Page\Model\TattsModel'       			  => __DIR__ . '/src/Page/Model/TattsModel.php',
      'Page\Model\PageModel'       			  => __DIR__ . '/src/Page/Model/PageModel.php',
      'Page\Controller\IndexController' 	=> __DIR__ . '/src/Page/Controller/IndexController.php',
	    'Page\Language\Lingo' 					    => __DIR__ . '/src/Page/Language/Lingo.php',
	    'Page\Language\de_DE' 					    => __DIR__ . '/src/Page/Language/de_DE.php',
	    'Page\Language\en_AU' 					    => __DIR__ . '/src/Page/Language/en_AU.php',
      'Page\Module'                     	=> __DIR__ . '/Module.php',  
);