<?php
namespace Api\Model;

use Zend\Db\Adapter\Adapter,
    Zend\Db\Sql\Sql,
    Zend\Db\Sql\Select,
    Zend\Db\Sql\Insert,
    Zend\Db\ResultSet\ResultSet,
    Zend\Mail\Message,
    Zend\Mime\Message as MimeMessage,
    Zend\Mime\Part as MimePart,
    Zend\Db\TableGateway\TableGateway,
    Zend\Mail\Transport\Smtp as SmtpTransport,
    Zend\Mail\Transport\SmtpOptions,
    Zend\Crypt\Password\Bcrypt,
	Application\Helper\HelperModel as SuperModel;
    

class CronModel extends SuperModel
{

    /**
     * @desc Helper class
     * @var object
     */
    protected $gHelper;
    
    /**
     * @desc Set errors
     * @var array
     */
    public $error = array();
    
    /**
     * @desc Set errors
     * @var array
     */
    public $msg = array();
    
	/**
	 * @desc Set environment, get injections, set stuff
	 * @param Adapter $adapter
	 * @param Helper $gHelper
	 */
    public function __construct(Adapter $adapter, $gHelper)
    {  
    	parent::__construct($adapter);
    	$this->dbAdapter	= $adapter;
    	$this->gHelper		= $gHelper;
		
    }
	
	/**
	 * @desc public function get the sport feed
	 */
	public function getSports()
	{
		$this->getAllSports();
		return $this->dataSports;
	}
	
	/**
	 * @desc update the datase every day
	 */
	public function updateDb(){
		$this->getAllSports();
		return $this->saveSports();
	}
	
	/**
	 * get/set all sports feed
	 * i must be able to set the feed in the class for later use and also to return it using the other class
	 */
	private function getAllSports()
	{	
		$feedUrl = 'https://tatts.com/pagedata/sports/sports.xml';
		$this->dataSports = $this->getDataStream($feedUrl);
	}
	
	/**
    * @desc get the data stream via curl
    * @param string $url
    * @return mixed
    */
	private function getDataStream ($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_FAILONERROR,1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		$data = curl_exec($ch);  
		
		if(curl_errno($ch)){
			echo 'error:' . curl_error($ch);
		}
		
		curl_close($ch);
		   
		// create xml object
		$xmlData = simplexml_load_string($data);
		// create an array to make it easier to get to the data
		$xmlData = json_decode(json_encode((array)$xmlData), 1);
		
		// return result array
		if(is_array($xmlData) && count($xmlData)){
			return $xmlData;
		}
		   
		return false;
	
	}
	
	/**
	 * @desc update the sport list
	 */
	private function saveSports()
	{
		$sportid = 0;
		
		if(is_array($this->dataSports) && count($this->dataSports)){
			
			foreach($this->dataSports['Sport'] AS $key => $value){
				
				// make sure the sport doesn't exists before insert
				$check = "SELECT 
							count(`sportid`) as counter, `sportid` 
								FROM `aff_sports`
							WHERE
								`tatts_sportsid` = ".(int)$value['@attributes']['SportId']." 
				";				
				
				$statement = $this->dbAdapter->query($check);
			    $result = $statement->execute(array());
			    $result = $this->gHelper->formResult($result);
				
				// new sport, yes, enter it
				if(isset($result[0]['counter']) && $result[0]['counter'] < 1) {
					
					$query = "INSERT INTO `aff_sports`
								(
								`name`, 
								`shortname`,
								`tatts_sportsid`
								)
								VALUES
								(
								'".$value['@attributes']['SportName']."',
								'".$value['@attributes']['SportNameShort']."',
								".(int)$value['@attributes']['SportId']."
								)
					";
				
					$this->dbAdapter->query($query)
						->execute(array());
						// need the sport id
					$sportid = $this->dbAdapter->getDriver()->getConnection()->getLastGeneratedValue();
				}else{
					// get sportid
					$sportid = $result[0]['sportid'];
				}
				
				// insert leagues
				if(isset($value['League']) && count($value['League'])){
					
					$this->saveLeagues($value['League'], $value['@attributes']['SportId'], $sportid);
					
					$this->saveMeetings($value['League']);
				}
			}
		}
		
		
	    $this -> setError('test');
	    return false;
			
	}

	/**
	 * @save leagues 
	 */
	private function saveLeagues($leagues, $tattsid, $sportsid)
	{
		$insertString = NULL;
		
		foreach($leagues AS $value) {
			
			if(isset($value['@attributes']) && array_key_exists('LeagueId',$value['@attributes'])) {
				
				// make sure the sport doesn't exists before insert
				$check = "SELECT 
							count(`id`) as counter
								FROM `aff_leagues`
							WHERE
								`leagueid` = ".(int)$value['@attributes']['LeagueId']." 
				";				
				
				$statement 	= $this->dbAdapter->query($check);
			    $result 	= $statement->execute(array());
			    $result 	= $this->gHelper->formResult($result);
				
				if(isset($result[0]['counter']) && $result[0]['counter'] < 1) {
				
					$query = "(
								".(int)$value['@attributes']['LeagueId'].",
								".(int)$sportsid.",
								".(int)$tattsid.",
								'".$value['@attributes']['LeagueName']."'
								),";
					
					$insertString .= $query;
				}
			}

		}

		if(!empty($insertString)){
			$string = substr($insertString, 0, -1);
			$string = "INSERT INTO `aff_leagues` (`leagueid`,`sportid`, `tatts_sportsid`,`name`) VALUES ".$string;
		
			try {
				$this->dbAdapter->query($string)
					->execute(array());
				} catch (\Zend\Db\Exception $e) {
					print $e;
			}
		}
	}

	/**
	 * @save meetings 
	 */
	private function saveMeetings($leagues)
	{
		$insertString = NULL;
		
		foreach($leagues AS $value) {
			
			$leagueid = NULL;
			
			if(isset($value['@attributes']) && array_key_exists('LeagueId',$value['@attributes'])) {
				$leagueid = $value['@attributes']['LeagueId'];	
			}
			
			if(isset($value['Meeting']) && count($value['Meeting'])) {
					
				foreach($value['Meeting'] AS $value) {
					
					$this->saveEventData($value['@attributes']['MeetingId']);
					
					// make sure the sport doesn't exists before insert
					$check = "SELECT 
								count(`id`) as counter
									FROM `aff_meetings`
								WHERE
									`meetingid` = ".$value['@attributes']['MeetingId']." 
					";
					
					$statement 	= $this->dbAdapter->query($check);
			    	$result 	= $statement->execute(array());
			    	$result 	= $this->gHelper->formResult($result);	
					
					if(isset($result[0]['counter']) && $result[0]['counter'] < 1) {
						$query = "(
								".(int)$leagueid.",
								".(int)$value['@attributes']['MeetingId'].",
								'".$value['@attributes']['MeetingName']."',
								'".$value['@attributes']['SortOrder']."'
								),";
					
						$insertString .= $query;
					}
				}
				
			} elseif(array_key_exists(0, $value) && count($value[0])) {
				
				// make sure the sport doesn't exists before insert
				$check = "SELECT 
							count(`id`) as counter
								FROM `aff_meetings`
							WHERE
								`meetingid` = ".$value[0]['@attributes']['MeetingId']." 
				";
				
				$statement 	= $this->dbAdapter->query($check);
		    	$result 	= $statement->execute(array());
		    	$result 	= $this->gHelper->formResult($result);	
				
				if(isset($result[0]['counter']) && $result[0]['counter'] < 1) {
					$query = "(
							".(int)$leagueid.",
							".(int)$value[0]['@attributes']['MeetingId'].",
							'".$value[0]['@attributes']['MeetingName']."',
							'".$value[0]['@attributes']['SortOrder']."'
							),";
				
					$insertString .= $query;
				}
			}	
		}

		if(!empty($insertString)){
			$string = substr($insertString, 0, -1);
			$string = "INSERT INTO `aff_meetings` (`leagueid`,`meetingid`, `name`,`sortorder`) VALUES ".$string;
		
			try {
				$this->dbAdapter->query($string)
					->execute(array());
				} catch (\Zend\Db\Exception $e) {
					print $e;
			}
		}
	}
	
	/**
	 * @desc save the event data
	 */
	private function saveEventData($meetingid){
		
		$meetingDetails = $this->getDataStream('https://tatts.com/pagedata/sports/meetings/meeting'.$meetingid.'.xml');
		$insertString = NULL;
		$venueString = NULL;
		
		if(is_array($meetingDetails['League']) && count($meetingDetails['League'])){
			if(is_array($meetingDetails['League']['Meeting']) && count($meetingDetails['League']['Meeting'])){
				if(array_key_exists('MainEvent', $meetingDetails['League']['Meeting']) && 
					is_array($meetingDetails['League']['Meeting']['MainEvent']) && 
						count($meetingDetails['League']['Meeting']['MainEvent'])){
					
					foreach($meetingDetails['League']['Meeting']['MainEvent'] AS $value) {
											
						if(is_array($value) && array_key_exists('@attributes', $value)){

							// enter new venues
							$this->saveVenueData($value);

							if(isset($value['@attributes']['MainEventId'])){
								
								// make sure the sport doesn't exists before insert
								$check = "SELECT 
											count(`id`) as counter
												FROM `aff_events`
											WHERE
												`eventid` = ".(int)$value['@attributes']['MainEventId']." 
								";
								
								$statement 	= $this->dbAdapter->query($check);
						    	$result 	= $statement->execute(array());
						    	$result 	= $this->gHelper->formResult($result);	
								
								if(!isset($value['@attributes']['AwayTeamId'])){
									$value['@attributes']['AwayTeamId'] = 0;
								}
								if(!isset($value['@attributes']['HomeTeamId'])){
									$value['@attributes']['HomeTeamId'] = 0;
								}
								
								// we dont want to have duplicates
								if(isset($result[0]['counter']) && $result[0]['counter'] < 1) {
									$query = "(
											 ".(int)$value['@attributes']['MainEventId'].",
											 ".(int)$meetingid.",
											 ".$this->dbAdapter->platform->quoteValue($value['@attributes']['EventName']).",
											 ".$this->dbAdapter->platform->quoteValue($value['@attributes']['EventNameShort']).",
											 ".$this->dbAdapter->platform->quoteValue($value['@attributes']['SortOrder']).",
											 ".$this->dbAdapter->platform->quoteValue($value['@attributes']['Status']).",
											 ".$this->dbAdapter->platform->quoteValue($value['@attributes']['EventStartTime']).",
											 ".$this->dbAdapter->platform->quoteValue($value['@attributes']['MenuName']).",
											 ".$this->dbAdapter->platform->quoteValue($value['@attributes']['HomeTeamId']).",
											 ".$this->dbAdapter->platform->quoteValue($value['@attributes']['AwayTeamId'])."
											 ),";
								
									$insertString .= $query;
								}
							}
							
						}
					}

					if(!empty($insertString)){
						$string = substr($insertString, 0, -1);
						$string = "INSERT INTO `aff_events` (`eventid`, `meetingid`, `name`, `nameshort`,`sortorder`,`status`,`starttime`,`menuname`,`hometeam`,`awayteam`) VALUES ".$string;
					
						try {

							$this->dbAdapter->query($string)
								->execute(array());
							} catch (\Zend\Db\Exception $e) {
								print $e;
						}
					}
				}
			}		
		}
		
		return false;
	}
	
	/**
	 * @desc save venue
	 */
	private function saveVenueData($value)
	{
		if(array_key_exists('Venue', $value) && is_array($value['Venue']) && count($value['Venue'])) {
								
			if(array_key_exists('@attributes', $value['Venue']) && is_array($value['Venue']['@attributes']) && 
				count($value['Venue']['@attributes'])) {
				
				// chech if venue already exists
				$check = "SELECT 
						count(`id`) as counter
							FROM `aff_venues`
						WHERE
							`venueid` = ".(int)$value['Venue']['@attributes']['VenueId']." 
				";
				
				$statement 	= $this->dbAdapter->query($check);
		    	$result 	= $statement->execute(array());
		    	$result 	= $this->gHelper->formResult($result);
		    	
				if(isset($result[0]['counter']) && $result[0]['counter'] < 1) {
					$query = "(
							 ".(int)$value['Venue']['@attributes']['VenueId'].",
							 ".$this->dbAdapter->platform->quoteValue($value['Venue']['@attributes']['VenueName']).",
							 ".$this->dbAdapter->platform->quoteValue($value['Venue']['@attributes']['VenueShortName']).",
							 ".$this->dbAdapter->platform->quoteValue($value['Venue']['@attributes']['LocationName']).",
							 ".$this->dbAdapter->platform->quoteValue($value['Venue']['@attributes']['LocationCode']).",
							 ".$this->dbAdapter->platform->quoteValue($value['Venue']['@attributes']['CountryName']).",
							 ".$this->dbAdapter->platform->quoteValue($value['Venue']['@attributes']['CountryCode'])."
							 )";
	
	
					$ven = "INSERT INTO `aff_venues` (`venueid`, `name`, `nameshort`, `locationname`,`locationcode`,`country`,`countrycode`) VALUES ".$query;
				
					try {
	
						$this->dbAdapter->query($ven)
							->execute(array());
						} catch (\Zend\Db\Exception $e) {
							print $e;
					}
				}
			}
		}	
	}
	
	public function getResults()
	{
		$results = $this->getDataStream('https://tatts.com/pagedata/sports/results.xml');
		$meetingDetails = $this->getDataStream('https://tatts.com/pagedata/sports/meetings/meeting'.$meetingid.'.xml');
	}
	
}
