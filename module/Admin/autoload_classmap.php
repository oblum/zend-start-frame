<?php
return array (
    'Admin\Model\AdminModel'       		 => __DIR__ . '/src/Admin/Model/AdminModel.php',
    'Admin\Controller\IndexController' => __DIR__ . '/src/Admin/Controller/IndexController.php',
	  'Admin\Language\Lingo' 					   => __DIR__ . '/src/Admin/Language/Lingo.php',
	  'Admin\Language\de_DE' 					   => __DIR__ . '/src/Admin/Language/de_DE.php',
	  'Admin\Language\en_AU' 					   => __DIR__ . '/src/Admin/Language/en_AU.php',
    'Admin\Module'                     => __DIR__ . '/Module.php',  
);