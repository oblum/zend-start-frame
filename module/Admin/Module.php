<?php

namespace Admin;

use Admin\Model\AdminModel,
    Application\Helper\HelperAdapter as HelperAdapter,
    Zend\Mvc\ModuleRouteListener,
    Zend\Db\Adapter\Adapter as DbAdapter,
	Admin\Controller\HelperModel as SuperModel;

class Module
{
	
	public function onBootstrap($e)
    {

    	
    }
    
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array (
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array (
                'namespaces' => array (
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
     
    
    public function getServiceConfig()
    {
        return array (
            'factories' => array ( 
                'helper-adapter' => function($sm) {
                    $helperAdapter = new HelperAdapter();
                    return $helperAdapter;
                },
                'admin-model' => function($sm) {
                    $config = $sm->get('config');
                    $config = $config['db'];
                    $adminModel = new Model\AdminModel(new DbAdapter($config), new HelperAdapter());
                    return $adminModel;
                },
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}