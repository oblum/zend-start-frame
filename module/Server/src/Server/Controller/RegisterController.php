<?php
/**
 * and Login Controller
 * @author blumanski (borg) <blumanski@gmail.com>
 * @package server 2
 * @date 10/07/2012
 */
// get bloddy request - //$this->getRequest()->getPost()->get('username');
namespace Server\Controller;

use Zend\Mvc\Controller\AbstractActionController, 
    Zend\View\Model\ViewModel, 
    Zend\InputFilter\InputFilter, 
    Zend\InputFilter\Input, 
    Zend\Validator, 
    Server\Model\AccountModel, 
    Server\Language\Lingo;

class RegisterController extends AbstractActionController 
{
  /** 
   * @desc Template Vars
   * @type array
   */
  private $tplVars = array();

  /**
   * @desc Error Messages
   * @type array
   */
  public $error = array();

  /**
   *@desc Helper Object
   * @var object
   */
  public $helperAdapter;

  /**
   *@desc Db table
   * @var object
   */
  protected $accountModel;

  /**
   * @desc language array
   * @var unknown_type
   */
  public $Lingo;

  /**
   * @desc index action
   */
  public function indexAction()
  {
    // @global controller set up
    $this -> setEnv();

    // @unique do activation if needed
    $this -> isActivated();

    // request process if available
    if ($this -> getRequest() -> isPost()) {

      // does the registration if needed
      $this -> registerUser();
      // does the user login if needed
      $this -> loginUser();
      // forgotten password
      $this -> forgotPassword();

    }

    // merge all errors onto one array
    $this -> mergeErrors();
    $this -> defineTplVars();

    // start standard view
    $view = new ViewModel($this -> tplVars);

    // error messages - assign to view
    $errMsg = new ViewModel( array('errmsg' => $this -> tplVars['error']));
    $errMsg -> setTemplate('server/global/errmsg');
    $this -> tplVars['error'] = array();
    $view -> addChild($errMsg, 'errmsg');

    // if sussess message is available - assign to view
    if ($this -> helperAdapter -> session -> success != '') {

      $sysMsg = new ViewModel( array('sysmsg' => $this -> helperAdapter -> session -> success));
      $sysMsg -> setTemplate('server/global/sysmsg');
      // set success message back to empty
      //$this->session->success = '';
      $view -> addChild($sysMsg, 'sysmsg');

    }

    return $view;
  }

  /**
   * @desc triggers the login process
   */
  private function forgotPassword()
  {
    if ($this -> getRequest() -> getPost() -> get('forgot')) {
      if ($this -> forgotProcess($this -> getRequest() -> getPost()) === true) {
        if ($this -> accountModel -> forgotUser($this -> getRequest() -> getPost() -> get('femail')) === true) {
          $this -> helperAdapter -> session -> success = $this -> Lingo -> String['successForgot'];
          return $this -> redirect() -> toUrl('/server');
        } else {
          
        }
      }
    }

    return false;

  }

  /**
   * @desc triggers the login process
   */
  private function loginUser()
  {
    if ($this -> getRequest() -> getPost() -> get('login')) {
      if ($this -> loginProcess($this -> getRequest() -> getPost()) === true) {

        $udata = $this -> accountModel -> loginUser($this -> getRequest() -> getPost());

        if ($udata === true && $this -> helperAdapter -> session -> udata[0]['noaco'] != 1) {
          return $this -> redirect() -> toUrl('/server/account/');
        } elseif ($this -> helperAdapter -> session -> udata[0]['noaco'] == 1) {
          return $this -> redirect() -> toUrl('/server/account/');
        }
      }
    }

    return false;

  }

  /**
   * @desc Trigger registration process and may redirect
   */
  private function registerUser()
  {
    if ($this -> getRequest() -> getPost() -> get('register')) {
      if ($this -> registerProcess($this -> getRequest() -> getPost()) === true) {
        if ($this -> accountModel -> addUser($this -> getRequest() -> getPost()) === true) {
          $this -> helperAdapter -> session -> success = $this -> Lingo -> String['accountcreated'];
          return $this -> redirect() -> toUrl('/server');
        }
      }
      $this -> tplVars['error'][] = $this -> Lingo -> String['registerfailed'];
    }

    return false;

  }


  /**
   * @desc trigger activation if needed
   */
  private function isActivated()
  {
    // is activation code available
    $activate = $this -> getEvent() -> getRouteMatch() -> getParam('key');

    // trigger check code
    if ($activate != '') {
      if ($this -> accountModel -> activateUser($activate) === true) {

        $this -> helperAdapter -> session -> success = $this -> Lingo -> String['activtrue'];
        return $this -> redirect() -> toUrl('/server');
      }
    }

    return false;

  }


  /**
   * @desc prepare the register process
   * @desc validate data
   * @param data array ($_POST)
   * @return bool
   */
  private function registerProcess($data)
  {
    if (strlen($data -> get('username')) < 1 || $data -> get('username') == $this -> Lingo -> String['userName']) {
      $this -> error[] = $this -> Lingo -> String['entername'];
    }

    if (!preg_match('/^[a-z\d-]{3,15}$/i', $data -> get('username'))) {
      $this -> error[] = $this -> Lingo -> String['invalusername'];
    }

    if ($data -> get('email') != $data -> get('email-confirm') && $data -> get('email') != '') {
      $this -> error[] = $this -> Lingo -> String['emailconfail'];
    }

    $validator = new \Zend\Validator\EmailAddress();
    if ($validator -> isValid($data -> get('email'))) {

    } else {

      $this -> error[] = $this -> Lingo -> String['invalemail'];
    }

    if ($data -> get('code') != '' && $data -> get('code') != 'Code' && !preg_match('/^[a-z\d_]{12}$/i', $data -> get('code'))) {
      $this -> error[] = $this -> Lingo -> String['invalcode'];
    }

    if (count($this -> error) < 1) {
      return true;
    }

    return false;

  }


  /**
   * @desc prepare data for login
   * @return bool
   */
  private function loginProcess($data)
  {
    if (strlen($data -> get('username')) < 1) {
      $this -> error[] = $this -> Lingo -> String['entername'];
    }

    if (!preg_match('/^[a-z\d_]{3,15}$/i', $data -> get('username'))) {
      $this -> error[] = $this -> Lingo -> String['invalusername'];
    }

    if (strlen($data -> get('password')) < 1) {
      $this -> error[] = $this -> Lingo -> String['enterpass'];
    }

    if (count($this -> error) < 1) {
      return true;
    }

    return false;

  }

  /**
   * @desc prepare data for login
   * @return bool
   */
  private function forgotProcess($data)
  {
    $validator = new \Zend\Validator\EmailAddress();

    if ($validator -> isValid($data -> get('femail'))) {
    } else {
      $this -> error[] = $this -> Lingo -> String['invalemail'];
    }

    if (count($this -> error) < 1) {
      return true;
    }

    return false;

  }

  /**
   * @desc define template vars to start
   */
  private function defineTplVars()
  {
    $this -> tplVars['error'] = $this -> error;
    $this -> tplVars['data'] = '';
    $this -> tplVars['lingo'] = $this -> Lingo -> String;

    return false;
  }

  /**
   * @desc set up dependencies
   */
  private function setEnv()
  {
    // get helper
    $this -> getHelper();
    // get model
    $this -> getModel();
    // get language
    $this -> getLingo('de_DE');
    // set lingo in model
    $this -> accountModel -> setLingo($this -> Lingo);

    $this -> layout = $this -> layout();
    $this -> layout -> lang = $this -> Lingo -> String;
  }

  /**
   * @desc merge the errors for printing
   */
  private function mergeErrors()
  {
    $this -> error = array_merge($this -> error, $this -> accountModel -> getError());
    $this -> tplVars['error'] = $this -> error;
    return false;
  }

  public function getModel()
  {
    if (!$this -> accountModel) {
      $sm = $this -> getServiceLocator();
      $this -> accountModel = $sm -> get('account-model');
    }
    return false;
  }

  public function getLingo($lang)
  {
    if (!$this -> Lingo) {
      $this -> Lingo = new Lingo($lang);
    }
    return false;
  }

  public function getHelper()
  {

    if (!$this -> helperAdapter) {
      $sm = $this -> getServiceLocator();
      $this -> helperAdapter = $sm -> get('helper-adapter');
    }
    return false;
  }

}
