<?php

namespace Api\Controller;

use Zend\Mvc\Controller\AbstractActionController, 
    Zend\View\Model\ViewModel, 
    Zend\Session\Container, 
    Zend\InputFilter\InputFilter, 
    Zend\InputFilter\Input, 
    Zend\Validator, 
    Api\Model\TattsModel, 
    Api\Language\Lingo;

class IndexController extends AbstractActionController
{

  /**
   * @desc Template Vars
   * @type array
   */
  private $tplVars = array();

  /**
   * @desc Error Messages
   * @type array
   */
  public $error = array();

  /**
   *@desc Helper Object
   * @var object
   */
  public $helperAdapter;

  public $cronModel;


/** ------------------------------------- Action controller -----------------*/

  /**
   * @des start of process
   */
  public function indexAction()
  {
    // @global controller set up
    $this->getHelper();
	$this->getCronModel();
	

	$sports = $this->cronModel->updateDb();
	print json_encode($sports);

    //$this->tplVars['data'] = array('I ', 'am ', 'working ', 'fine. ');
	
	//$this->tplVars['sports'] = $sports;

    //$view = new ViewModel($this->tplVars);
    //$view->setTemplate('api/index/index');

    //return $view;
  }


  /** ---------------------------- Controller set up ---------------------*/

  
   /**
   * @desc define template vars to start
   */
  private function defineTplVars()
  {
    $this->tplVars['error'] = $this->error;
    $this->tplVars['data'] = '';
    $this->tplVars['lingo'] = $this->Lingo->String;

    return false;
  }
  
  /**
   * @desc set up dependencies
   */
  private function setEnv()
  {
    // get helper
    $this->getHelper();
    // get language
    $this->getLingo('de_DE');
    // set lingo in model
    //$this->cronModel->setLingo($this->Lingo);

    $this->layout = $this->layout();
   // $this->layout->lang = $this->Lingo->String;
  }


  /**
   * @desc merge the errors for printing
   */
  private function mergeErrors()
  {
    //$this->error = array_merge($this->error, $this->cronModel->getError());
    //$this->tplVars['error'] = $this->error;
   // return false;
  }

  
  public function getCronModel()
  {
    if (!$this->cronModel) {
      $sm = $this->getServiceLocator();
      $this->cronModel = $sm->get('cron-model');
    }
    return false;
  }


  public function getLingo($lang)
  {
    if (!isset($this->Lingo)) {
      $this->Lingo = new Lingo($lang);
    }
    return false;
  }


  public function getHelper()
  {

    if (!$this->helperAdapter) {
      $sm = $this->getServiceLocator();
      $this->helperAdapter = $sm->get('helper-adapter');
    }
    return false;
  }

}
