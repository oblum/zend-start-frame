<?php

namespace Server\Controller;

use Zend\Mvc\Controller\AbstractActionController,
	  Zend\View\Model\ViewModel,
	  Zend\Session\Container,
	  Zend\InputFilter\InputFilter,
	  Zend\InputFilter\Input,
	  Zend\Validator,
	  Server\Model\AccountModel,
	  Server\Language\Lingo;

class BackendController extends AbstractActionController 
{
    /**
     * @desc Template Vars
     * @type array
     */
    private $tplVars 	= array();
    
    /**
     * @desc Error Messages
     * @type array
     */
    public  $error	= array();
    
    /**
     *@desc Helper Object
     * @var object 
     */
    public  $helperAdapter;
    
    /**
     *@desc Db table
     * @var object 
     */
    protected $accountModel;
	
	/**
	 * @des start of process
	 */
    public function indexAction() 
    {
    // @global controller set up
		$this->setEnv();
		
		
		// merge all errors onto one array
    $this->mergeErrors();
    $this->defineTplVars();
    // start standard view
		$view = new ViewModel($this->tplVars);
		 
		
		$errMsg = new ViewModel(array('errMsg' => $this->tplVars['error']));
		$errMsg->setTemplate('server/global/errmsg');
		$this->tplVars['error'] = array();
		$view->addChild($errMsg, 'errmsg');
		
		// success handling
//     	if($this->helperAdapter->session->success != ''){
    		
//     		$sysMsg = new ViewModel(array('sysmsg' => $this->helperAdapter->session->success));
//         	$sysMsg->setTemplate('server/global/sysmsg');
//         	$this->helperAdapter->session->success = '';
//         	$view->addChild($sysMsg, 'sysmsg');
    		
//     	}

        return $view;
    }   
    
    /**
     * @des start of process
     */
    public function adminAction() 
    {
    	// @global controller set up
    	$this->setEnv();
    	$this->defineTplVars();
    
    	$this->mergeErrors();
    	$view = new ViewModel($this->tplVars);
    
    	$errMsg = new ViewModel(array('errMsg' => $this->tplVars['error']));
    	$errMsg->setTemplate('server/global/errmsg');
    	$this->tplVars['error'] = array();
    	$view->addChild($errMsg, 'errmsg');
    
    	// success handling
    	if($this->helperAdapter->session->success != ''){
    		$sysMsg = new ViewModel(array('sysmsg' => $this->helperAdapter->session->success));
    		$sysMsg->setTemplate('server/global/sysmsg');
    		$this->helperAdapter->session->success = '';
    		$view->addChild($sysMsg, 'sysmsg');
    	}
    
    	return $view;
    }
    

    /**
     * @desc define template vars to start
     */
    private function defineTplVars() 
    {
    	$this->tplVars['error']		= $this->error;
    	$this->tplVars['data']		= '';
    	$this->tplVars['lingo'] 	= $this->Lingo->String;
    	 
    	return false;
    }
    

    /**
     * @desc set up dependencies
     */
    private function setEnv()
    {
    	// get helper
    	$this->getHelper();
    	// new session name space
    	$this->helperAdapter->setNewSessionContainer('backsess', 'backend');
    	print '<pre>';
    	print_r($this->helperAdapter->backsess);
    	print '</pre>';
    	// get model
    	$this->getModel();
    	// get backend model
    	$this->getBackendModel();
    	// get language
    	$this->getLingo('de_DE');
    	// set lingo in model
    	$this->accountModel->setLingo($this->Lingo);
    	$this->backendModel->setLingo($this->Lingo);
    	
    	$this->layout = $this->layout();
    	$this->layout->lang = $this->Lingo->String;
    }
    

    /**
     * @desc merge the errors for printing
     */
    private function mergeErrors()
    {
    	$this->error = array_merge($this->error, $this->accountModel->getError());
    	$this->tplVars['error'] = $this->error;
    	return false;
    }
    

    public function getBackendModel() 
    {
    	if (!$this->backendModel) {
    		$sm = $this->getServiceLocator();
    		$this->backendModel = $sm->get('backend-model');
    	}
    	return false;
    }
    
    
    public function getModel() 
    {
      if (!$this->accountModel) {
          $sm = $this->getServiceLocator();
          $this->accountModel = $sm->get('account-model');
      }
     return false;
    } 
    
    
    public function getLingo($lang) 
    {
    	if (!$this->Lingo) {
    		$this->Lingo = new Lingo($lang);
    	}
    	return false;
    }
    
    
    public function getHelper() 
    {
        
      if (!$this->helperAdapter) {
          $sm = $this->getServiceLocator();
          $this->helperAdapter = $sm->get('helper-adapter');
      }
      return false;
    } 
    
}
