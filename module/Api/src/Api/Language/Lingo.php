<?php

namespace Api\Language;

use Api\Language\de_DE, 
	Api\Language\en_AU;

class Lingo {
	
	public $String;
	
	public function __construct($lang){
		
		if($lang == 'de_DE'){
			$p = new de_DE();
		}elseif($lang == 'en_AU'){
			$p = new en_AU();
		}else{
			$p = new de_DE();
		}
		
		$this->String = $p->String;
			
	}
// -----------------
	
}
