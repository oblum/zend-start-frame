<?php
return array(
    'Server\Model\AccountModel'       			=> __DIR__ . '/src/Server/Model/AccountModel.php',
	'Server\Model\MatorModel'       			=> __DIR__ . '/src/Server/Model/MatorModel.php',
    'Server\Controller\IndexController' 		=> __DIR__ . '/src/Server/Controller/IndexController.php',
    'Server\Controller\RegisterController' 		=> __DIR__ . '/src/Server/Controller/RegisterController.php',
	'Server\Controller\AccountController' 		=> __DIR__ . '/src/Server/Controller/AccountController.php',
	'Server\Controller\MatorController' 		=> __DIR__ . '/src/Server/Controller/MatorController.php',
	'Server\Language\Lingo' 					=> __DIR__ . '/src/Server/Language/Lingo.php',
	'Server\Language\de_DE' 					=> __DIR__ . '/src/Server/Language/de_DE.php',
	'Server\Language\en_AU' 					=> __DIR__ . '/src/Server/Language/en_AU.php',
    'Server\Module'                     		=> __DIR__ . '/Module.php',  
);