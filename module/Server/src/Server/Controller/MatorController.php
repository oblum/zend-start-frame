<?php

namespace Server\Controller;

use Zend\Mvc\Controller\AbstractActionController, 
    Zend\View\Model\ViewModel, 
    Zend\Session\Container, 
    Zend\InputFilter\InputFilter, 
    Zend\InputFilter\Input, 
    Zend\Validator, 
    Server\Model\AccountModel, 
    Server\Language\Lingo;

class MatorController extends AbstractActionController
{

 /**
   * @desc Template Vars
   * @type array
   */
  private $tplVars = array();

  /**
   * @desc Error Messages
   * @type array
   */
  public $error = array();

  /**
   *@desc Helper Object
   * @var object
   */
  public $helperAdapter;

  /**
   *@desc Db table
   * @var object
   */
  protected $accountModel;

  /**
   *@desc Mator Model
   * @var object
   */
  protected $matorModel;

  /**
   * @des start of process
   */
  public function indexAction()
  {
    // @global controller set up
    $this -> setEnv();

    if ($this -> helperAdapter -> isAdminLoggedIn() === true) {
      return $this -> redirect() -> toUrl('/server/mator/dash/');
    }

    // does the user login if needed
    $this -> loginUser();

    // merge all errors onto one array
    $this -> mergeErrors();
    $this -> defineTplVars();
    // start standard view
    $view = new ViewModel($this -> tplVars);
    $this -> layout('/../../../public/templates/mator/index.phtml');

    $errMsg = new ViewModel( array('errMsg' => $this -> tplVars['error']));
    $errMsg -> setTemplate('server/global/errmsg');
    $this -> tplVars['error'] = array();
    $view -> addChild($errMsg, 'errmsg');

    // 		$this->tplVars['lingo'] = $this->Lingo->String;
    // 		$logtpl = new ViewModel($this->tplVars);
    // 		$logtpl->setTemplate('server/mator/login/');
    // 		$view->addChild($logtpl, 'login');

    return $view;
  }


  /**
   * @des start of process
   */
  public function dashAction()
  {

    // @global controller set up
    $this -> setEnv();

    if ($this -> helperAdapter -> isAdminLoggedIn() !== true) {
      return $this -> redirect() -> toUrl('/server/mator/');
    }

    // merge all errors onto one array
    $this -> mergeErrors();
    $this -> defineTplVars();
    // start standard view
    $view = new ViewModel($this -> tplVars);
    $this -> layout('layout/mator/index.phtml');

    $errMsg = new ViewModel( array('errMsg' => $this -> tplVars['error']));
    $errMsg -> setTemplate('server/global/errmsg');
    $this -> tplVars['error'] = array();
    $view -> addChild($errMsg, 'errmsg');

    $this -> tplVars['lingo'] = $this -> Lingo -> String;
    $dashtpl = new ViewModel($this -> tplVars);
    $dashtpl -> setTemplate('server/mator/dash/');
    $view -> addChild($dashtpl, 'data');

    return $view;

  }


  /**
   * @desc triggers the login process
   */
  private function loginUser()
  {

    if ($this -> getRequest() -> getPost() -> get('login')) {
      if ($this -> loginProcess($this -> getRequest() -> getPost()) === true) {
        $adata = $this -> matorModel -> loginAdmin($this -> getRequest() -> getPost());
        if ($adata === true && $this -> helperAdapter -> matorsess -> adata['in'] != 1) {
          return $this -> redirect() -> toUrl('/server/mator/');
        } elseif ($adata === true && $this -> helperAdapter -> matorsess -> adata['in'] == 1) {
          return $this -> redirect() -> toUrl('/server/mator/dash/');
        }
      }
    }

    return false;

  }

  // ------------------ Validation ----------------

  /**
   * @desc prepare data for login
   * @return bool
   */
  private function loginProcess($data)
  {
    if (strlen($data -> get('username')) < 1) {
      $this -> error[] = $this -> Lingo -> String['entername'];
    }

    if (!preg_match('/^[a-z\d_]{3,15}$/i', $data -> get('username'))) {
      $this -> error[] = $this -> Lingo -> String['invalusername'];
    }

    if (strlen($data -> get('password')) < 1) {
      $this -> error[] = $this -> Lingo -> String['enterpass'];
    }

    if (count($this -> error) < 1) {
      return true;
    }

    return false;

  }


  /**
   * @desc define template vars to start
   */
  private function defineTplVars()
  {
    $this -> tplVars['error'] = $this -> error;
    $this -> tplVars['data'] = '';
    $this -> tplVars['lingo'] = $this -> Lingo -> String;

    return false;
  }


  /**
   * @desc set up dependencies
   */
  private function setEnv()
  {
    // get helper
    $this -> getHelper();
    $this -> helperAdapter -> setMatorContainer();
    // get model
    $this -> getModel();
    // get backend model
    $this -> getMatorModel();
    // get language
    $this -> getLingo('de_DE');
    // set lingo in model
    $this -> accountModel -> setLingo($this -> Lingo);
    $this -> matorModel -> setLingo($this -> Lingo);

    $this -> layout = $this -> layout();
    $this -> layout -> lang = $this -> Lingo -> String;
  }


  /**
   * @desc merge the errors for printing
   */
  private function mergeErrors()
  {
    $this -> error = array_merge($this -> error, $this -> accountModel -> getError());
    $this -> tplVars['error'] = $this -> error;
    return false;
  }


  public function getMatorModel()
  {
    if (!$this -> matorModel) {
      $sm = $this -> getServiceLocator();
      $this -> matorModel = $sm -> get('mator-model');
    }
    return false;
  }


  public function getModel()
  {
    if (!$this -> accountModel) {
      $sm = $this -> getServiceLocator();
      $this -> accountModel = $sm -> get('account-model');
    }
    return false;
  }


  public function getLingo($lang)
  {
    if (!$this -> Lingo) {
      $this -> Lingo = new Lingo($lang);
    }
    return false;
  }


  public function getHelper()
  {

    if (!$this -> helperAdapter) {
      $sm = $this -> getServiceLocator();
      $this -> helperAdapter = $sm -> get('helper-adapter');
    }
    return false;
  }

}
