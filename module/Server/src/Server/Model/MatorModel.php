<?php
namespace Server\Model;

use Zend\Db\Adapter\Adapter,
    Zend\Db\Sql\Sql,
    Zend\Db\Sql\Select,
    Zend\Db\Sql\Insert,
    Zend\Db\ResultSet\ResultSet,
    Zend\Mail\Message,
    Zend\Mime\Message as MimeMessage,
    Zend\Mime\Part as MimePart,
    Zend\Db\TableGateway\TableGateway,
    Zend\Mail\Transport\Smtp as SmtpTransport,
    Zend\Mail\Transport\SmtpOptions,
    Zend\Crypt\Password\Bcrypt,
	  Application\Helper\HelperModel as SuperModel;
    

class MatorModel extends SuperModel
{

    /**
     * @desc Helper class
     * @var object
     */
    protected $gHelper;
    
    /**
     *@desc db table users
     * @var array
     */
    public $tableUsers 		= array();
    
    /**
     * @desc db table for accounts
     * @var array
     */
    public $tableAccounts 	= array();
    
    /**
     * clear password
     * @var string
     */
    public $clearPassword	= NULL;
    
    /**
     * @desc Set errors
     * @var array
     */
    public $error = array();
    
    /**
     * @desc Set errors
     * @var array
     */
    public $msg = array();
    
    private $password = '08fivezehn';
    
    private $username = 'matrinski';


	/**
	 * @desc Set environment, get injections, set stuff
	 * @param Adapter $adapter
	 * @param Helper $gHelper
	 */
    public function __construct(Adapter $adapter, $gHelper)
    {  
    	parent::__construct($adapter);
    	$this->dbAdapter	= $adapter;
    	$this->gHelper		= $gHelper;
    	$this->gHelper->setMatorContainer();
    	
    }
     

    /**
     * @desc Simple login, no db no fuzz
     * @param string $usrname
     * @param string $usrpassword
     */
    public function loginAdmin($data)
    {
    	if($data->get('username') == $this->username && $data->get('password') == $this->password){
    		$this->gHelper->sessExpire();
    		$mat = array(
    				'in'	=> 1,
    				'timer' => time()
    		);
    		
    		$this->gHelper->toMatorSession('adata',$mat);
    		
    		return true;
    		
    }
    	
    	$this->setError($this->Lingo->String['loginfail']);
    	 
    	return false;

    }


}
