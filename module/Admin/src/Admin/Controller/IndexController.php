<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController, 
    Zend\View\Model\ViewModel, 
    Zend\Session\Container, 
    Zend\InputFilter\InputFilter, 
    Zend\InputFilter\Input, 
    Zend\Validator, 
    Admin\Model\AdminModel, 
    Admin\Language\Lingo;

class IndexController extends AbstractActionController
{

  /**
   * @desc Template Vars
   * @type array
   */
  private $tplVars = array();

  /**
   * @desc Error Messages
   * @type array
   */
  public $error = array();

  /**
   *@desc Helper Object
   * @var object
   */
  public $helperAdapter;

  /**
   *@desc Db table
   * @var object
   */
  protected $adminModel;


/** ------------------------------------- Action controller -----------------*/

  /**
   * @des start of process
   */
  public function indexAction()
  {
    // must be in each controller action
    $this->setEnv();
    
    // make sure user is logged in
    if($this->helperAdapter->isLogged() !== true){
      $this->helperAdapter->Logout($this->redirect()->toUrl('/server/register/'));
    }
    
    // @global controller set up
    $this->setEnv();
    $this->mergeErrors();
    $this->defineTplVars();
    
    $this->tplVars['data'] = array('I ', 'am ', 'working ', 'fine. ');
    $this->tplVars['helper'] = $this->helperAdapter;

    $view = new ViewModel($this->tplVars);
    $view->setTemplate('admin/index/index');

    return $view;
  }










  /** ---------------------------- Controller set up ---------------------*/

  
   /**
   * @desc define template vars to start
   */
  private function defineTplVars()
  {
    $this->tplVars['error'] = $this->error;
    $this->tplVars['data'] = '';
    $this->tplVars['lingo'] = $this->Lingo->String;

    return false;
  }
  
  /**
   * @desc set up dependencies
   */
  private function setEnv()
  {
    
    // get helper
    $this->getHelper();
    // get model
    $this->getModel();
    // get language
    $this->getLingo('de_DE');
    // set lingo in model
    $this->adminModel->setLingo($this->Lingo);

    $this->layout = $this->layout();
    $this->layout->lang = $this->Lingo->String;
  }


  /**
   * @desc merge the errors for printing
   */
  private function mergeErrors()
  {
    $this->error = array_merge($this->error, $this->adminModel->getError());
    $this->tplVars['error'] = $this->error;
    return false;
  }


  public function getModel()
  {
    if (!$this->adminModel) {
      $sm = $this->getServiceLocator();
      $this->adminModel = $sm->get('admin-model');
    }
    return false;
  }
  


  public function getLingo($lang)
  {
    if (!isset($this->Lingo)) {
      $this->Lingo = new Lingo($lang);
    }
    return false;
  }


  public function getHelper()
  {

    if (!$this->helperAdapter) {
      $sm = $this->getServiceLocator();
      $this->helperAdapter = $sm->get('helper-adapter');
    }
    return false;
  }

}
