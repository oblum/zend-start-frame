jQuery(document).ready(function(){
	
	
	if($('button#inviteNew')){
		$('#gencode').hide();
		$('button#inviteNew').click(function(){
			$('button#inviteNew').hide();
			$('#gencode').show();
		});
	}

	if($('#show-register')){
		
		if($.cookie("tabi") != 'register'){
			$('#reg').hide();
		}else{
			$('#log').hide();
		}
		
		$('#show-register').click(function(){
			
			$('#log').hide();
			$('#reg').show('fade');
			$.cookie("tabi", "register");
			
		});
		
		$('#show-login').click(function(){
			
			$('#reg').hide();
			$('#log').show('fade');
			$.cookie("tabi", "login");
			
		});
                
           $('#register-form').submit(function(){
               
                // register form validation
                var uname   = $('[name="username"]').val();
                var email   = $('[name="email"]').val();
                var confirm = $('[name="email-confirm"]').val();
                var code    = $('[name="code"]').val();
                // uname
		if(uname == '' || uname == 'Username'){    
			$.alerts.dialogClass = $(this).attr('id'); // set custom style class
				jAlert('Please enter your desired username', 'Form Error', function() {
				$.alerts.dialogClass = null; // reset to default
			});
			return false;
		}
        // uname
        if(uname.length > 15 || uname.length < 4){
        	$.alerts.dialogClass = $(this).attr('id'); // set custom style class
				jAlert('Enter a valid username', 'Form Error', function() {
				$.alerts.dialogClass = null; // reset to default
			});
			return false;
        }
        if (!uname.match(/^[0-9a-zA-Z\-'_]+$/)){     
            $.alerts.dialogClass = $(this).attr('id'); // set custom style class
				jAlert('Username has invalid letters.', 'Form Error', function() {
				$.alerts.dialogClass = null; // reset to default
			});
			return false;
        }
        // email
        if(email != confirm && email != '' && email != 'Email'){
            $.alerts.dialogClass = $(this).attr('id'); // set custom style class
				jAlert('The email confirmation is invalid', 'Form Error', function() {
				$.alerts.dialogClass = null; // reset to default
			});
			return false;
        }
        // code
        if(code != 'Code' && code != ''){
            if(code.length != 12){
               $.alerts.dialogClass = $(this).attr('id'); // set custom style class
					jAlert('Please enter a valid code or leave the field empty.', 'Form Error', function() {
					$.alerts.dialogClass = null; // reset to default
				});
				return false;
            }
        }
		// email
		if(email == '' || email == 'Email'){
			$.alerts.dialogClass = $(this).attr('id'); // set custom style class
				jAlert('Please enter your email', 'Form Error', function() {
				$.alerts.dialogClass = null; // reset to default
			});
			return false;
		}
		/** mail validation*/
		if( !isValidEmailAddress( email ) ) {
			$.alerts.dialogClass = $(this).attr('id'); // set custom style class
				jAlert('Please enter a valid email address', 'Form Error', function() {
				$.alerts.dialogClass = null; // reset to default
			});
			return false;
		}
           });
           // register form end
	}
        // end show-register
	
	if($('.question')) {
		
            $('.question').click(function(){

                    $(this).parent().find('.info:first').show('fade');

            });
            // hide box if click outside
            $('html').click(function() {
                    $('.info').fadeOut('slow');
            });
            // stop propagation
            $('.question').click(function(event){
                    event.stopPropagation();
                });
            $('.info').click(function(event){
                    event.stopPropagation();
                });

	}
	
});

// ----------------------------

function isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
	return pattern.test(emailAddress);
};

var isInteger_re     = /^\s*(\+|-)?\d+\s*$/;
function isInteger (s) {
   return String(s).search (isInteger_re) != -1
}


