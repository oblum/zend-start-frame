<?php
namespace Server\Model;

use Zend\Db\Adapter\Adapter, 
    Zend\Db\Sql\Sql, 
    Zend\Db\Sql\Select, 
    Zend\Db\Sql\Insert, 
    Zend\Db\ResultSet\ResultSet, 
    Zend\Mail\Message, 
    Zend\Mime\Message as MimeMessage, 
    Zend\Mime\Part as MimePart, 
    Zend\Db\TableGateway\TableGateway, 
    Zend\Mail\Transport\Smtp as SmtpTransport, 
    Zend\Mail\Transport\SmtpOptions, 
    Zend\Crypt\Password\Bcrypt, 
    Application\Helper\HelperModel as SuperModel;

class AccountModel extends SuperModel
{

  /**
   * @desc Helper class
   * @var object
   */
  protected $gHelper;

  /**
   *@desc db table users
   * @var array
   */
  public $tableUsers = array();

  /**
   * @desc db table for accounts
   * @var array
   */
  public $tableAccounts = array();

  /**
   * clear password
   * @var string
   */
  public $clearPassword = NULL;

  /**
   * @desc Set errors
   * @var array
   */
  public $error = array();

  /**
   * @desc Set errors
   * @var array
   */
  public $msg = array();

  /**
   * @desc Set environment, get injections, set stuff
   * @param Adapter $adapter
   * @param Helper $gHelper
   */
  public function __construct(Adapter $adapter, $gHelper)
  {
    parent::__construct($adapter);
    $this -> gHelper = $gHelper;
    // table users vars
    $this -> setTableUsersVars();
    $this -> setTableAccountVars();
    $this -> setTableAssignerTable();
  }


  /**
   * @desc Add user processor
   * @param object $data
   */
  public function loginUser($data)
  {
    //check user login
    if ($this -> doLogin($data -> get('username'), $data -> get('password')) === true) {
      return true;
    } else {
      if ($this -> doLoginSecAttempt($data -> get('username'), $data -> get('password')) === true) {
        return true;
      }
    }

    return false;
  }

  // ------------------
  /**
   * @desc Add user processor
   * @param object $data
   */
  public function forgotUser($email)
  {
    return $this -> forgotPasswordReset($email);
  }

  // ------------------
  /**
   * @desc Log user out
   * @param object $redirect
   */
  public function logout($usrid, $router)
  {
    // delete user from logtable
    $this -> deleteUserFromLogged($usrid);
    // logout
    // redirect, only if session
    if ($router !== false) {
      $this -> gHelper -> Logout($router);
    }
  }

  // ------------------
  /**
   * @desc Check if enaugh masters are assigned to account before resigning
   * @param int $acoid
   */
  private function getMasterCountOfAccount($acoid, $all = false)
  {
    $m = NULL;
    if ($all === false) {
      $m = "`asipermissions` = 69 AND";
    }

    $query = "SELECT count(asiid) FROM `assigner` WHERE " . $m . " `asiacoid` = " . (int)$acoid;

    $statement = $this -> dbAdapter -> query($query);
    $results = $statement -> execute(array());

    $result = $this -> gHelper -> formResult($results);

    if (array_key_exists(0, $result) && array_key_exists('counter', $result[0])) {

      if ($result[0]['counter'] > 1) {
        return true;
      }
    }

    $this -> setError($this -> Lingo -> String['error86']);

    return false;

  }


  /**
   * @desc Update code on call
   * @param int $acoid
   */
  private function updateCode($acoid)
  {
    $code = $this -> getCode();
    $query = "UPDATE `accounts` 
    				SET `acocode` = " . $this -> dbAdapter -> platform -> quoteValue($code) . " 
    					WHERE
    						`acoid` = " . (int)$acoid . "
    	";

    $statement = $this -> dbAdapter -> query($query);
    $result = $statement -> execute(array());

    if ($result -> getAffectedRows() > 0) {
      return true;
    }

    return false;

  }

 
  /**
   * @desc Get account permissions for a user in account
   * @param int $usrid
   * @param int $acoid
   */
  private function getUsersAccountPermissions($usrid, $acoid)
  {
    $query = "SELECT `asipermissions` FROM assigner WHERE `asiusrid` = " . (int)$usrid . " AND `asiacoid` = " . (int)$acoid . "";

    $statement = $this -> dbAdapter -> query($query);
    $results = $statement -> execute(array());
    $result = $this -> gHelper -> formResult($results);

    if (array_key_exists(0, $result)) {
      return $result[0]['asipermissions'];
    }

    $this -> setError($this -> Lingo -> String['error85']);

    return false;

  }


  /**
   * @desc Get accountdata by email address
   * @param string email
   */
  private function forgotPasswordReset($email)
  {
    $newPwd = $this -> gHelper -> friendly_name();
    $query = "UPDATE `users` 
    				SET `usrpassword` = " . $this -> dbAdapter -> platform -> quoteValue($this -> encryptPassword($newPwd)) . "
    					WHERE
    						`usremail` = " . $this -> dbAdapter -> platform -> quoteValue($email) . "
    	";

    $statement = $this -> dbAdapter -> query($query);
    $result = $statement -> execute(array());

    if ($result -> getAffectedRows() > 0) {
      if ($this -> sendPasswordForgottenMessage($email, $newPwd) === true) {
        return true;
      }
    }

    $this -> setError($this -> Lingo -> String['error90']);

    return false;

  }


  /**
   * resign user from account, done by master
   * @param int $acoid
   * @param int $usrid
   */
  public function resignUser($acoid, $usrid)
  {
    if ($this -> gHelper -> isMaster() === true) {

      $perms = $this -> getUsersAccountPermissions($usrid, $acoid);
      // no perms, no user
      if ($perms < 1) {
        $this -> setError($this -> Lingo -> String['notInThisAccount']);
        return false;
      }

      if (($perms == 69 && $this -> getMasterCountOfAccount($acoid, true) === true) || ($perms != 69 && $this -> getMasterCountOfAccount($acoid) === true)) {
        $query = "DELETE FROM assigner WHERE `asiacoid` = " . (int)$acoid . " AND `asiusrid` = " . (int)$usrid . "";
        $statement = $this -> dbAdapter -> query($query);
        $result = $statement -> execute(array());
        if ($result -> getAffectedRows() > 0) {
          // regenerate new code as resigned user could know the old one
          $this -> updateCode($acoid);

          return true;
        }
      }
    }

    return false;

  }


  /**
   * set user to master of account with all rights
   * @param int $acoid
   * @param int $usrid
   */
  public function userToMaster($acoid, $usrid)
  {
    if ($this -> gHelper -> isMaster() === true) {
      $query = "UPDATE `assigner` SET `asipermissions` = 69 WHERE `asiacoid` = " . (int)$acoid . " AND `asiusrid` = " . (int)$usrid . "";
      $statement = $this -> dbAdapter -> query($query);
      $result = $statement -> execute(array());

      if ($result -> getAffectedRows() > 0) {
        // user will be logged out and can login with new permissions
        $this -> logout($usrid, false);
        return true;
      }
    }

    return false;

  }


  /**
   * set user to master of account with all rights
   * @param int $acoid
   * @param int $usrid
   */
  public function userToPlayer($acoid, $usrid)
  {
    if ($this -> gHelper -> isMaster() === true) {
      if ($this -> getMasterCountOfAccount($acoid) === true) {
        $query = "UPDATE `assigner` SET `asipermissions` = 1 WHERE `asiacoid` = " . (int)$acoid . " AND `asiusrid` = " . (int)$usrid . "";

        $statement = $this -> dbAdapter -> query($query);
        $result = $statement -> execute(array());

        if ($result -> getAffectedRows() > 0) {
          // user will be logged out and can login with new permissions
          $this -> logout($usrid, false);

          return true;
        }
      }
    }

    return false;

  }


  /**
   * @desc Add user processor
   * @param object $data
   */
  public function addUser($data)
  {
    //check unique username and email
    if ($this -> uniqueUsername($data -> get('username'), $data -> get('email')) === true) {
      // create user for user without code and inclusive account
      if ($data -> get('code') == '' || $data -> get('code') == 'Code') {
        // process db entry
        if ($this -> processAddUser($data) === true) {
          // send email to user
          return $this -> sendMessage($this -> tableUsers['usremail'], $this -> tableUsers['usrname'], $this -> clearPassword, $this -> tableUsers['usractivate']);
        }
      } else {

        // assign user
        $assignId = $this -> getAccountIdByCode($data -> get('code'));
        if ((int)$assignId > 0) {
          // process db entry
          if ($this -> processAssignUser($data, $assignId) === true) {
            // send email to user
            return $this -> sendMessage($this -> tableUsers['usremail'], $this -> tableUsers['usrname'], $this -> clearPassword, $this -> tableUsers['usractivate']);
          }
        }
      }
    }

    return false;

  }


  /**
   * @desc check activation key and move on
   * @param array $data
   */
  public function activateUser($code)
  {
    if ($code == '') {
      return false;
    }

    $sql = "SELECT 
    					count(usractivate) as counter, usrid 
    				FROM `users` WHERE `usractivate` = " . $this -> dbAdapter -> platform -> quoteValue(substr($code, 0, 16)) . "
    				AND `usrstatus` = 0
		";

    $statement = $this -> dbAdapter -> query($sql);
    $results = $statement -> execute(array());
    $result = $this -> gHelper -> formResult($results);

    if (array_key_exists(0, $result) && array_key_exists('counter', $result[0])) {
      if ($result[0]['counter'] == 1) {
        if ($this -> updateUserStatus($result[0]['usrid'], 1) === true) {
          $this -> setMsg($this -> Lingo -> String['activetrue']);
          return true;
        }
      }
    }

    $this -> setError($this -> Lingo -> String['activefalse']);

    return false;

  }

 
  /**
   * @desc update the user status - 0 not activated
   * @desc 1 all good, 2 banned
   * @param unknown_type $user
   * @param unknown_type $status
   */
  private function updateUserStatus($user, $status)
  {
    $sql = "UPDATE `users` 
					SET 
						`usrstatus` = " . (int)$status . ",
						`usractivate` = '' 
					WHERE 
						`usrid` = " . (int)$user . "
		";
    $statement = $this -> dbAdapter -> query($sql);
    $result = $statement -> execute(array());

    if ($result -> getAffectedRows() > 0) {
      return true;
    }

    return false;

  }

 
  /**
   * @desc Logout
   */
  public function isLogged()
  {
    if ($this -> session -> udata[0]['usrid'] > 0) {
      return true;
    }
    return false;
  }


  /**
   * @desc Second login attempt for users which are not assigned to an account yet
   * @param string $usrname
   * @param string $usrpassword
   */
  public function doLoginSecAttempt($usrname, $usrpassword)
  {
    $sql = "SELECT a.*
			    	FROM users a
			    	WHERE
			    	`usrname` = " . $this -> dbAdapter -> platform -> quoteValue($usrname) . "
			    	AND
			    	`usrpassword` = " . $this -> dbAdapter -> platform -> quoteValue($this -> encryptPassword($usrpassword)) . "
			    	AND 
			    		a.usrstatus != 0    	
		";

    $statement = $this -> dbAdapter -> query($sql);
    $results = $statement -> execute(array());
    $result = $this -> gHelper -> formResult($results);

    if (is_array($result) && count($result)) {

      $result[0]['expire'] = time() + $this -> gHelper -> sessExpire;
      $result[0]['noaco'] = 1;
      $this -> gHelper -> toSession('udata', $result);
      // add to permanent logtable
      $this -> addToLoggedLog(0, $result[0]['usrid']);
      // add to temporaer log table
      $this -> addToLogged(0, $result[0]['usrid']);

      return true;

    }

    $this -> setError($this -> Lingo -> String['loginfail']);

    return false;

  }


  /**
   * @desc First login attempt, this is loogin in only users which are assigned to an account
   * @param string $usrname
   * @param string $usrpassword
   */
  public function doLogin($usrname, $usrpassword)
  {
    $sql = "SELECT a.*,
    				   c.*,
    				   b.asipermissions
			    	FROM users a
			    		LEFT JOIN assigner b
			    			ON a.usrid = b.asiusrid
			    		LEFT JOIN accounts c
			    			ON b.asiacoid = c.acoid
			    	WHERE
			    	`usrname` = " . $this -> dbAdapter -> platform -> quoteValue($usrname) . "
			    	AND
			    	`usrpassword` = " . $this -> dbAdapter -> platform -> quoteValue($this -> encryptPassword($usrpassword)) . "
			    	AND 
			    		b.asiusrid = a.usrid
			    	AND 
			    		b.asiacoid = c.acoid 
			    	AND 
			    		a.usrstatus != 0    	
		";

    $statement = $this -> dbAdapter -> query($sql);
    $results = $statement -> execute(array());
    $result = $this -> gHelper -> formResult($results);

    if (is_array($result) && count($result)) {

      $result[0]['expire'] = time() + $this -> gHelper -> sessExpire;
      $this -> gHelper -> toSession('udata', $result);
      $aco = array('acoid' => $result[0]['acoid'], 'aconame' => $result[0]['aconame'], 'acostatus' => $result[0]['acostatus'], 'acocode' => $result[0]['acocode']);

      // add to permanent logtable
      $this -> addToLoggedLog($result[0]['acoid'], $result[0]['usrid']);
      // add to temporaer log table
      $this -> addToLogged($result[0]['acoid'], $result[0]['usrid']);
      $this -> gHelper -> toSession('account', $aco);
      
      return true;

    }

    $this -> setError($this -> Lingo -> String['loginfail']);

    return false;

  }


  /**
   * @desc check if the user is already assigned to an account
   * @param int $usrid
   */
  private function isNotAssigned($usrid)
  {
    $query = "SELECT count(asiid) FROM `assigner` WHERE `asiusrid` = " . (int)$usrid;

    $statement = $this -> dbAdapter -> query($query);
    $results = $statement -> execute(array());
    $result = $this -> gHelper -> formResult($results);

    if (array_key_exists(0, $result) && array_key_exists('counter', $result[0])) {
      if ($result[0]['counter'] < 1) {
        return true;
      }
    }

    $this -> setError($this -> Lingo -> String['assignAlready']);

    return false;

  }


  /**
   * @desc Delete the userlogin data from the login table
   * @param int $usrid
   */
  private function deleteUserFromLogged($usrid)
  {
    $query = "DELETE FROM `game_loggedin` WHERE `loggusrid` = " . (int)$usrid;
    $statement = $this -> dbAdapter -> query($query);
    $result = $statement -> execute(array());

    if ($result -> getAffectedRows() > 0) {
      return true;
    }

    return false;

  }


  /**
   * @desc Look if the user is in the login table and if the session was running out
   * @param int $usrid
   */
  public function areAccountPlayerOnline($acoid)
  {

    $query = "SELECT loggusrid FROM `game_loggedin` WHERE `loggacoid` = " . (int)$acoid . "";
    $statement = $this -> dbAdapter -> query($query);
    $results = $statement -> execute(array());
    $result = $this -> gHelper -> formResult($results);

    if (array_key_exists(0, $result)) {
      $tmp = array();

      foreach ($result as $key => $val) {
        $tmp[$val['loggusrid']] = $val['loggusrid'];
      }

      return $tmp;

    }

    $this -> setError($this -> Lingo -> String['error89']);

    return false;

  }


  /**
   * @desc Look if the user is in the login table and if the session was running out
   * @param int $usrid
   */
  public function isUserOnline($usrid)
  {
    $query = "SELECT count(loggid) AS counter FROM `game_loggedin` WHERE `loggusrid` = " . (int)$usrid . "";

    $statement = $this -> dbAdapter -> query($query);
    $results = $statement -> execute(array());
    $result = $this -> gHelper -> formResult($results);

    if (array_key_exists(0, $result) && array_key_exists('counter', $result[0])) {

      if ($result[0]['counter'] > 0) {
        return true;
      }
    }

    $this -> setError($this -> Lingo -> String['error87']);

    return false;

  }


  /**
   * @desc Look if the user is in the login table and if the session was running out
   * @param int $usrid
   */
  public function isExpiredCheck($usrid)
  {
    $query = "SELECT count(loggid) AS counter FROM `game_loggedin` WHERE `loggusrid` = " . (int)$usrid . " AND `loggout` > " . (int)time() . "";

    $statement = $this -> dbAdapter -> query($query);
    $results = $statement -> execute(array());
    $result = $this -> gHelper -> formResult($results);

    if (array_key_exists(0, $result) && array_key_exists('counter', $result[0])) {
      if ($result[0]['counter'] > 0) {
        return false;
      }
    }

    return true;

  }


  /**
   * @desc add user to logged in table
   * @param int $acoid
   * @param int $usrid
   */
  private function addToLogged($acoid, $usrid)
  {
    $exp = time() + $this -> gHelper -> sessExpire;
    $this -> deleteUserFromLogged($usrid);

    $query = "INSERT INTO `game_loggedin` 
    				(loggacoid, loggusrid, loggtime, loggout)
    				 VALUES
    				  	(" . (int)$acoid . ", " . (int)$usrid . ", " . time() . ", $exp)
    	";

    $statement = $this -> dbAdapter -> query($query);
    $result = $statement -> execute(array());

    if ($result -> getAffectedRows() > 0) {
      return true;
    }

    return false;

  }


  /**
   * @desc add user permanent logtable
   * @param int $acoid
   * @param int $usrid
   */
  private function addToLoggedLog($acoid, $usrid)
  {
    $query = "INSERT INTO `log_logins` 
    				(loggacoid, loggusrid, loggtime)
    			VALUES
    				(" . (int)$acoid . ", " . (int)$usrid . ", " . time() . ")
    	";

    $statement = $this -> dbAdapter -> query($query);
    $result = $statement -> execute(array());

    if ($result -> getAffectedRows() > 0) {
      return true;
    }

    return false;

  }


  /**
   * @desc Send message after account is created
   * @param string $email
   * @param string $username
   * @return bool
   */
  private function sendPasswordForgottenMessage($email, $password)
  {
    $transport = new SmtpTransport();
    $options = new SmtpOptions( array('name' => 'smtpout.asia.secureserver.net', 'connection_class' => 'login', 'connection_config' => array('username' => 'oliver@bloomo.com.au', 'password' => 'Dbpwbhko07', 'ssl' => 'ssl'), 'host' => 'smtpout.asia.secureserver.net', 'port' => 465, ));
    $transport -> setOptions($options);

    $text = new MimePart('Plain text');
    $text -> type = "text/plain";

    $html = new MimePart($this -> Lingo -> String['forgottenMsg'] . ",  <br />" . $this -> Lingo -> String['password'] . ": " . $password . " <br /><br />" . $this -> Lingo -> String['goodby']);
    $html -> type = "text/html";

    $body = new MimeMessage();
    $body -> setParts(array($text, $html));

    $message = new Message();
    $message -> setEncoding("UTF-8");
    $message -> addTo($email) -> addFrom('oliver@bloomo.com.au', 'Oliver Blum') -> addReplyTo("oliver@bloomo.com.au", "Oliver") -> setSubject($this -> Lingo -> String['forgotsubj']) -> setBody($body);

    $transport -> send($message);

    return true;

  }


  /**
   * @desc Send message after account is created
   * @param string $email
   * @param string $username
   * @return bool
   */
  private function sendMessage($email, $username, $password, $activationCode)
  {
    $transport = new SmtpTransport();
    $options = new SmtpOptions( array('name' => 'smtpout.asia.secureserver.net', 'connection_class' => 'login', 'connection_config' => array('username' => 'oliver@bloomo.com.au', 'password' => 'Dbpwbhko07', 'ssl' => 'ssl'), 'host' => 'smtpout.asia.secureserver.net', 'port' => 465, ));
    $transport -> setOptions($options);

    $text = new MimePart('Plain text');
    $text -> type = "text/plain";

    $html = new MimePart("User: " . $username . " <br />Password: " . $password . " <br /><br />Activation Link:  http://game.australia-locations.org/server/register/activate/" . $activationCode . "<br /><br />Cheers");
    $html -> type = "text/html";

    $body = new MimeMessage();
    $body -> setParts(array($text, $html));

    $message = new Message();
    $message -> setEncoding("UTF-8");
    $message -> addTo($email) -> addFrom('oliver@bloomo.com.au', 'Oliver Blum') -> addReplyTo("oliver@bloomo.com.au", "Oliver") -> setSubject('Account creation Server 2: ' . $username) -> setBody($body);

    $transport -> send($message);

    return true;

  }


  /**
   * @desc process account creation
   * @param object $data
   */
  private function processAssignUser($data, $assignId)
  {
    $this -> clearPassword = $this -> gHelper -> friendly_name();

    $this -> tableUsers['usrname'] = $data -> get('username');
    $this -> tableUsers['usremail'] = $data -> get('email');
    $this -> tableUsers['usrcreated'] = date('Y-m-d H:i:s');
    $this -> tableUsers['usrstatus'] = 0;
    $this -> tableUsers['usrpassword'] = $this -> encryptPassword($this -> clearPassword);
    $this -> tableUsers['usractivate'] = md5($this -> encryptPassword($this -> clearPassword . time()));

    try {
      // begin transaction
      $this -> dbAdapter -> getDriver() -> getConnection() -> beginTransaction();

      $acLastId = $this -> dbAdapter -> getDriver() -> getConnection() -> getLastGeneratedValue();
      // create user
      $this -> processDbInsert('users', $this -> tableUsers);
      $usLastId = $this -> dbAdapter -> getDriver() -> getConnection() -> getLastGeneratedValue();
      // config assigner entry
      $this -> tableAssigner['asiacoid'] = (int)$assignId;
      $this -> tableAssigner['asiusrid'] = $usLastId;
      $this -> tableAssigner['asipermissions'] = 1;
      // do assign account to user
      $this -> processDbInsert('assigner', $this -> tableAssigner);
      $newCode = $this -> getCode();
      $query = "UPDATE `accounts` SET `acocode` = " . $this -> dbAdapter -> platform -> quoteValue($newCode) . " WHERE `acoid` = " . (int)$assignId;
      $statement = $this -> dbAdapter -> query($query);
      $statement -> execute(array());
      // commit transaction
      $this -> dbAdapter -> getDriver() -> getConnection() -> commit();

      return true;

    } catch (\Zend\Db\Exception $e) {

      // rollback
      $this -> dbAdapter -> getDriver() -> getConnection() -> rollback();

      $this -> setError($e);
      return $e;

    }

    return false;

  }


  /**
   * @desc process account creation
   * @param object $data
   */
  private function processAddUser($data)
  {
    $this -> clearPassword = $this -> gHelper -> friendly_name();
    $this -> tableUsers['usrname'] = $data -> get('username');
    $this -> tableUsers['usremail'] = $data -> get('email');
    $this -> tableUsers['usrcreated'] = date('Y-m-d H:i:s');
    $this -> tableUsers['usrstatus'] = 0;
    $this -> tableUsers['usrpassword'] = $this -> encryptPassword($this -> clearPassword);
    $this -> tableUsers['usractivate'] = md5($this -> encryptPassword($this -> clearPassword . time()));

    try {
      // begin transaction
      $this -> dbAdapter -> getDriver() -> getConnection() -> beginTransaction();

      // create account
      $this -> processDbInsert('accounts', $this -> tableAccounts);
      $acLastId = $this -> dbAdapter -> getDriver() -> getConnection() -> getLastGeneratedValue();
      // create user
      $this -> processDbInsert('users', $this -> tableUsers);
      $usLastId = $this -> dbAdapter -> getDriver() -> getConnection() -> getLastGeneratedValue();
      // config assigner entry
      $this -> tableAssigner['asiacoid'] = $acLastId;
      $this -> tableAssigner['asiusrid'] = $usLastId;
      $this -> tableAssigner['asipermissions'] = 69;

      // do assign account to user
      $this -> processDbInsert('assigner', $this -> tableAssigner);
      // commit transaction
      $this -> dbAdapter -> getDriver() -> getConnection() -> commit();

      return true;

    } catch (\Zend\Db\Exception $e) {

      // rollback
      $this -> dbAdapter -> getDriver() -> getConnection() -> rollback();

      $this -> setError($e);
      return $e;

    }

    return false;

  }


  /**
   * @des Test if username is existing already
   * @param string $usrname
   * @param string $usremail
   * @return bool
   */
  public function uniqueUsername($usrname, $usremail)
  {
    $sql = "SELECT count(usrid) as counter 
			FROM users 
			WHERE 
				`usrname` = " . $this -> dbAdapter -> platform -> quoteValue($usrname) . " 
			OR 
				`usremail` = " . $this -> dbAdapter -> platform -> quoteValue($usremail);

    $statement = $this -> dbAdapter -> query($sql);
    $results = $statement -> execute(array());
    $result = $this -> gHelper -> formResult($results);

    if (array_key_exists(0, $result) && array_key_exists('counter', $result[0])) {

      if ($result[0]['counter'] < 1) {
        return true;
      }
    }

    $this -> setError($this -> Lingo -> String['inuse']);

    return false;

  }

  // ------------------
  /**
   * @desc Get account id fro provided code
   * @param string $code
   */
  private function getAccountIdByCode($code)
  {
    $query = "SELECT acoid FROM `accounts` WHERE `acocode` = " . $this -> dbAdapter -> platform -> quoteValue($code);

    $statement = $this -> dbAdapter -> query($query);
    $results = $statement -> execute(array());
    $result = $this -> gHelper -> formResult($results);

    if (is_array($result) && count($result)) {
      return $result[0]['acoid'];
    }

    $this -> setError($this -> Lingo -> String['error659']);

    return false;

  }

  
  /**
   * @desc Get all account data
   * @param int $acoid
   */
  public function getAccountData($acoid)
  {
    $query = "SELECT a.*
					FROM `accounts` a
					WHERE a.acoid = " . (int)$acoid . "
		";

    $statement = $this -> dbAdapter -> query($query);
    $results = $statement -> execute(array());
    $result = $this -> gHelper -> formResult($results);

    if (is_array($result) && count($result)) {
      $this -> gHelper -> toSession('acodata', $result);
      return true;
    }

    $this -> setError('Error: 658');

    return false;

  }


  /**
   * @desc Get all assigned users of an account
   * @param int $acoid
   */
  public function getAccountUsers($acoid)
  {
    $query = "SELECT a.*,
						 b.*
					FROM `users` a
						RIGHT JOIN `assigner` b
							ON a.usrid = b.asiusrid
					WHERE b.asiacoid = " . (int)$acoid . "
		";

    $statement = $this -> dbAdapter -> query($query);
    $results = $statement -> execute(array());
    $result = $this -> gHelper -> formResult($results);

    if (is_array($result) && count($result)) {
      return $result;
    }

    $this -> setError($this -> Lingo -> String['accountnousers']);

    return false;

  }


  /**
   * @desc Get new generated code for the next user for assigning
   */
  public function getCode()
  {
    return $this -> gHelper -> generateStrongPassword(12);
  }


  /**
   * @desc bcrypt encryption to store passwords
   * @param string $password
   */
  private function encryptPassword($password)
  {
    $bcrypt = new Bcrypt();
    $bcrypt -> setSalt('BYV-AL+%7%%s mr=');
    return $bcrypt -> create($password);

  }


  private function setTableAssignerTable()
  {
    $this -> tableAssigner = array('asicreated' => date('Y-m-d H:i:s'), 'asiacoid' => '', 'asiusrid' => '', 'asicodeused' => '', 'asipermissions' => '', );
  }


  private function setTableAccountVars()
  {
    $this -> tableAccounts = array('acocode' => $this -> getCode(12), 'aconame' => 'NoName', 'acocreated' => date('Y-m-d H:i:s'), 'acostatus' => '0', );
  }


  private function setTableUsersVars()
  {
    $this -> tableUsers = array('usrname' => '', 'usremail' => '', 'usrgender' => '', 'usrbirthday' => '', 'usrdescription' => '', 'usrmedia' => '', 'usrcreated' => '', 'usrlastrefresh' => '', 'usrstatus' => '', 'usrpassword' => '', 'usractivate' => '', );
  }

}
