<?php

namespace Server\Language;

class en_AU {
	
	public $String;
	
	public function __construct(){
		
		$this->String =  array(
		
			'welcome'			=> 'Welcome',
			'register'			=> 'Registration',
			'join'				=> 'Register',
			'login'				=> 'Login',
			'gameName'			=> 'Piratenkampf Server 2',
			'userName'			=> 'Username',
			'email'				=> 'eMail',
			'goodby'			=> 'See you later <br /><br />The Piratenkampf Team.',
			'emailConfirm'		=> 'Confirm eMail',
			'code'				=> 'Code',
			'password'			=> 'Password',
			'showHome'			=> 'Home',
			'showStarted'		=> 'Get Started',
			'showLogin'			=> 'Login',
			'showForum'			=> 'Forum',
			'showHistory'		=> 'Game History',
			'showRegister'		=> 'Sign up',
			'forgotsubj'		=> 'Piratenkampf - New Password',
			'helpCode'			=> 'You need only a code if you want to join another account. If you have no code just leave the field as it is.',
			'activtrue'			=> 'Your account is active now.',
			'activefalse'		=> 'Te activation code is invalid or already used.',
			'loginfail'			=> 'The login failed.',
			'inuse'				=> 'Username or email address are already in use.',
			'accountcreated'	=> 'Your Account is created, please check your email account and follow the instructions.',
			'registerfailed'	=> 'The registration process has failed.',
			'entername'			=> 'Plaese enter a username.',
			'invalusername'		=> 'The username is not alowed, allowed are letters and numbers as well as "-", the username must be between 3 and 15 charakters.',
			'emailconfail'		=> 'Email confirmation failed.',
			'invalemail'		=> 'Please enter a valid email address.',
			'invalcode'			=> 'The entered code is wrong.',
			'enterpass'			=> 'Please enter a password.',
			'accountnousers'	=> 'The account is not assigned to a user. Email to blumanski@gmail.com',
			'naviConf'			=> 'Configuration',
			'naviUsers'			=> 'Users',
			'naviEmail'			=> 'eMail',
			'naviExit'			=> 'Logout',
			'error659'			=> 'The provided code is not invalid.',
			'invitingCode'		=> 'Next inviting code',
			'invitingButton'	=> 'Invite Co Player',
			'assignAlready'		=> 'This user is already assigned to another account',
			'accoMaster'		=> 'Master',
			'accoPlay'			=> 'Player',
			'delete'			=> 'Delete',
			'toAdmin'			=> 'Admin status',
			'toPlayer'			=> 'Player status',
			'optionCode'		=> 'Join Account using code',
			'optionNew'			=> 'Start new Account',
			'noAccount'			=> 'You are not assigned to any account.',
			'noAccountSub'		=> 'Choose from two options, you can create a new account or you can use a code to join an existing account.',
			'notInThisAccount'	=> 'A player with that id is not in this account.',
			'error85'			=> 'Error: 85 - An error occured.',
			'error86'			=> 'Error: 86 - An error occured.',
			'error87'			=> 'Error: 87 - An error occured.',
			'error88'			=> 'Error: 88 - An error occured.',
			'error89'			=> 'Error: 89 - An error occured.',
			'admLogin'			=> 'Administrator Login',
			'admSearch'			=> 'Search',
			'admSearchUser'		=> 'Seach Player',
			'admSearchAccount'	=> 'Search Account',
			'admObject'			=> 'search',
			'defObject'			=> 'Isle',
			'requestPassword'	=> 'Request Password',
			'forgotPassword'	=> 'Lost Password',
			'forgottenMsg'		=> 'Hi, see below your new password fpr Piratenkampf.de',
			'successForgot'		=> 'Please check you email account, the new password is on the way.',
			'admUsers'			=> 'Players',
			'admAccounts'		=> 'Accounts',
			'admServerConf'		=> 'Server Config',
				
			'gettingHead'			=> 'Getting started',
			'gettingStartedShort'	=> '<p>See below some points to point out what the game is about.
			A world has may 36 oceans and the oceans lined up in a square just like you see below.</p>
				
<pre>

01|02|03|04|05|06|
07|08|09|10|11|12|
13|14|15|16|17|18|
19|20|21|22|23|24|
25|26|27|28|29|30|
31|32|33|34|35|36|

</pre>
			<p>
			Each ocean has 100 island groups and each island group contains up to 4 isles. That is how the Piratenkampf world looks like, aim of the game is to take over the world and conquer your way through the oceans.
			<br /><br /><a href="/server/beschreibung/" title="Read more">Read more</a></p>',
				
				
			'gettingStarted'	=> '<p>See below some points to point out what the game is about.
			A world has may 36 oceans and the oceans lined up in a square just like you see below.</p>
			
<pre>
	
01|02|03|04|05|06|
07|08|09|10|11|12|
13|14|15|16|17|18|
19|20|21|22|23|24|
25|26|27|28|29|30|
31|32|33|34|35|36|
	
</pre>
			<p>	
			Each ocean has 100 island groups and each island group contains up to 4 isles. That is how the Piratenkampf world looks like, aim of the game is to take over the world and conquer your way through the oceans.
			</p>
			<p>
			Each ocen has up to 50 players and each of em is starting with one island. During the game you will colonise or conquer more and more islands.
			</p>
			<p>
			Per usual the players of each ocean form an allicance to gain the might over their home ocean and save their teritorry.
			</p>
			<p>
			An alliance (wing) can have up to 20 members and the members should be all in the same location. It is very common that an alliance has two wings (same alliance with slightly different name) to be able to take more members on board.</p>
			<p>
			This is happening eventually in each ocean, new allicances form and inter ocean bonds between allicances will get formed.
			</p>
			<p>
			One alliance alone can\'t conquer the world alone, it takes strategic and diplomatic skills to conquer the world. To find the right alliance partners is a fundamental part of the game and each world creates a new sovial network.
			</p>
			<p>
			The next world is starting on the 16/09/2012 at 2pm.</p>',
				
				
				
			'txtWelcome'		=> '<h3>Piratenkampf is presenting</h3>
				<h4>
				World 1 - Classic Style
				</h4>
				<p>
				<strong>
				Start: Sonday, 16. September 2012 at 2pm.
				</strong>
				</p>
				<p>
				All players signing up before September 23 2012 8pm are getting the premium functionality until September 30 2012 for free.
				</p>
				
<pre>

01|02|03|04|05|06|
07|08|09|10|11|12|
13|14|15|16|17|18|
19|20|21|22|23|24|
25|26|27|28|29|30|
31|32|33|34|35|36|

</pre>
				<p>
				Oceans: 36 (6x6) (not all oceans can get settled to start with.), 50 player per ocean<br />
				Start Island: 1 Island, pre build<br />
				Ocean choice: No<br />
				Guard Tower: yes<br />
				Catapult: yes<br />
				Noob safety: no<br />
				Member per alliance: 20 incl. Administrators<br />
				Premium: yes<br />
				Lows: to start 75 lows (empty islands) per ocean, optional more added later<br />
				Holiday function: yes<br />
				Support: yes<br />
				Hunting for cheaters: yes<br />
				Colonization tool: Oceanview by KayG<br />
				English language: yes<br />
				Advertise Free: yes<br />
				</p>
				<p>
				Informations on the Premium Account functionalities
				</p>
				<p>
				Runtime (only on one world)<br />
				6 month 8&euro;<br />
				12 month 14&euro;<br />
				</p>
				<p>
				Special runtime (multiple worlds)<br />
				18 month 20&euro;<br />
				24 month 24&euro;<br />
				A life long XXL 99&euro;<br />
				</p>
				<p>
				Premium functionalities:
				</p>
				<ul>
				<li>Change of user name</li>
				<li>Add user and alliance image</li>
				<li>Write messages to all alliance members at once.</li>
				<li>Easy switch between islands (forward and backward)</li>
				<li>30 days holiday sitter</li>
				<li>Advertise free</li>
				<li>Able to delete account</li>
				<li>Autobuild of Units (running once a day)</li>
				<li>Autobuild buildings (running all 30 minutes)</li>
				</ul>
				',
				
				'txtHistory'		=> 'Best alliance ever was "dieKippe".',
				
				'navScreens'		=> 'Screenshots',
				'txtScreens'		=> 'All screenshots in here',
				'navFAQ'			=> 'FAQ',
				'txtFaq'			=> 'All FAQ in here',
				'navContact'		=> 'Kontakt',
				'txtContact'		=> 'Kontakt is in here',
				

		);
		
	}

}