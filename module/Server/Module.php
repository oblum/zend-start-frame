<?php

namespace Server;

use Server\Model\AccountModel,
    Application\Helper\HelperAdapter as HelperAdapter,
    Zend\Mvc\ModuleRouteListener,
    Zend\Db\Adapter\Adapter as DbAdapter,
	  Application\Controller\HelperModel as SuperModel;

class Module
{
	
	public function onBootstrap($e)
    {

    	
    }
    
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
     
    
    public function getServiceConfig()
    {
        return array(
            'factories' => array( 
                'helper-adapter' => function($sm) {
                    $helperAdapter = new HelperAdapter();
                    return $helperAdapter;
                },
                'mator-model' => function($sm) {
                    $config = $sm->get('config');
                    $config = $config['db'];
                    $matorModel = new Model\MatorModel(new DbAdapter($config), new HelperAdapter());
                    return $matorModel;
                },
//                'server-table' => function($sm) {
//                    $config = $sm->get('config');
//                    $config = $config['db'];
//                    $serverTable = new serverTable(new DbAdapter($config));
//                    return $serverTable;
//                },
                 'account-model' => function($sm) {
                    $config = $sm->get('config');
                    $config = $config['db'];
                    $accountModel = new Model\AccountModel(new DbAdapter($config), new HelperAdapter());
                    return $accountModel;
                },
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }
}