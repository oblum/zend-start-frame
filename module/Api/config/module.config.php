<?php
return array (
    'router' => array (
        'routes' => array (
            'api' => array (
                'type'    => 'Literal',
                'options' => array (
                    'route'    => '/api',
                    'defaults' => array (
                        'controller'    => 'Api\Controller\Index',
                        'action'        => 'index',
                     ),
                    
                    'show' => array (
                        'type' => 'Segment',
                        'options' => array (
                            'route'    => 'show[/:show]',
                            'defaults' => array (
                                'controller' => 'Api\Controller\Index',
                                'action'     => 'index'
                            ),
                        ),
                    ),
                ),	
            ),	
        ),
    ),
		
    'controllers' => array(
        'invokables' => array(
            'Api\Controller\Index' 		=> 'Api\Controller\IndexController',
        ),
    ),
    
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'api/index/index' 		=> __DIR__ . '/../view/page/index/index.phtml',
            'api/error/404'        => __DIR__ . '/../view/page/error/404.phtml',
            'api/error/index'      => __DIR__ . '/../view/page/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);


