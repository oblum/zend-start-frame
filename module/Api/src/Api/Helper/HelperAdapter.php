<?php

namespace Api\Helper;

use Zend\Session\SessionManager,
	Zend\Session\Container,
	Api\Language\Lingo;

class HelperAdapter 
{
	
	public $session;
	public $sessExpire = 30;
	public $registry;
    
  public function __construct(){
      
  	$this->session  = new Container('base');
  	$this->session->getManager()->regenerateId();

  }
  
  
  public function isAdminLoggedIn(){
  	
  	if($this->helperAdapter->matorsess->adata['in'] == 1)
  	{
  		return true;
  	}
  	return false;
  }
   
   
/**
 * @desc is session expired?
 */
  public function isExpired()
  {
  	if($this->session->udata[0]['expire'] < time()) {
  		return true;
  	}
  	
  	return false;
  }
  
  
/**
 * @desc Is user logged in?
 */
  public function isLogged(){
  	if($this->session->udata[0]['usrid'] > 0)
  	{
  		return true;
  	}
  	return false;
  }
  
/**
 * @desc quick validation tool to see if master or not
 */
  public function isMaster()
  {
  	if($this->session->udata[0]['asipermissions'] == 69) {
  		return true;
  	}
  	
  	return false;
  	
  }
  

/**
 * @desc Logput
 */
  public function Logout($router)
  {
  	if($this->session->udata[0]['usrid'] > 0) {
  		$this->session->udata = array();
  		unset($this->session->udata);
  		$this->session->getManager()->destroy();
  		return $router;
  	}

  	return $router;
  }
  

  public function sessExpire()
  {
  	$this->session->setExpirationSeconds($this->sessExpire);
  }
  
  
  
  public function toSession($key, $value)
  {
  	$this->session->$key = $value;
  }
  
  
  public function genCode()
  {
      echo 'DRINNE';
    }
    
 
    public function generateCode()
    {
			
    }

    public function formResult($results)
    {
    	$result = array();
		
		foreach ($results as $key => $value) {
		    $result[$key] = $value;
		}
		
		return $result;
    	
    }
    
    public function get3dDistance($x1, $y1, $z1, $x2, $y2, $z2)
    {
        $x1 = (float)$x1;
        $y1 = (float)$y1;
        $z1 = (float)$z1;
        $x2 = (float)$x2;
        $y2 = (float)$y2;
        $z2 = (float)$z2; 
        
        return sqrt(pow(($x2 - $x1), 2) + pow(($y2 - $y1), 2) + pow(($z2 - $z1),2));
    }

    
    function friendly_name($length = 10, $easy = false)
    {
        $consonants = array('b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','z');
      if($easy === true){
      	$vowels = array('a','e','i','O','u','y');
      }else{
      	$vowels = array('A','e','i','O','u','y', '*', '-', '+');
      }
      $pwd = '';

      for($i = 0; $i < $length; $i++){
          $pwd .= ($i % 2) === 0 ? 
                  $consonants[array_rand($consonants)] : 
                  $vowels[array_rand($vowels)]; 
      }

      return $pwd;   
  }
    
    
  function generateStrongPassword($length = 9, $add_dashes = false, $available_sets = 'luds')
  {
  	$sets = array();
  	if(strpos($available_sets, 'l') !== false)
  		$sets[] = 'abcdefghjkmnpqrstuvwxyz';
  	if(strpos($available_sets, 'u') !== false)
  		$sets[] = 'ABCDEFGHJKMNPQRSTUVWXYZ';
  	if(strpos($available_sets, 'd') !== false)
  		$sets[] = '23456789';
  	if(strpos($available_sets, 's') !== false)
  		$sets[] = '!@#$%&*?';
  
  	$all = '';
  	$password = '';
  	foreach($sets as $set){
  		$password .= $set[array_rand(str_split($set))];
  		$all .= $set;
  	}
  
  	$all = str_split($all);
  	for($i = 0; $i < $length - count($sets); $i++)
  		$password .= $all[array_rand($all)];
  
  	$password = str_shuffle($password);
  
  	if(!$add_dashes)
  		return $password;
  
  	$dash_len = floor(sqrt($length));
  	$dash_str = '';
  	while(strlen($password) > $dash_len){
  		$dash_str .= substr($password, 0, $dash_len) . '-';
  		$password = substr($password, $dash_len);
  	}
  	$dash_str .= $password;
  	return $dash_str;
 }
  
    
}