<?php
return array (
    'router' => array (
        'routes' => array (
            'admin' => array (
                'type'    => 'Literal',
                'options' => array (
                    'route'    => '/admin',
                    'defaults' => array (
                        'controller'    => 'Admin\Controller\Index',
                        'action'        => 'index',
                     ),
                    
                    'show' => array (
                        'type' => 'Segment',
                        'options' => array (
                            'route'    => 'show[/:show]',
                            'defaults' => array (
                                'controller' => 'Admin\Controller\Index',
                                'action'     => 'index'
                            ),
                        ),
                    ),
                ),	
            ),	
        ),
    ),
		
    'controllers' => array(
        'invokables' => array(
            'Admin\Controller\Index' 		=> 'Admin\Controller\IndexController',
        ),
    ),
    
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'admin/index/index' 		=> __DIR__ . '/../view/admin/index/index.phtml',
            'admin/error/404'        => __DIR__ . '/../view/admin/error/404.phtml',
            'admin/error/index'      => __DIR__ . '/../view/admin/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
);


