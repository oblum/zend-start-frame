<?php

namespace Server\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel,
    Zend\Session\Container,
    Zend\InputFilter\InputFilter,
    Zend\InputFilter\Input,
    Zend\Validator,
    Server\Model\AccountModel,
    Server\Language\Lingo;

class AccountController extends AbstractActionController 
{
	
	 /**
   * @desc Template Vars
   * @type array
   */
   private $tplVars 	= array();
    
   /**
    * @desc Error Messages
    * @type array
    */
   public  $error	= array();
    
   /**
   *@desc Helper Object
   * @var object 
   */
   public  $helperAdapter;
    
   /**
    *@desc Db table
    * @var object 
    */
   protected $accountModel;
	
	 /**
	 * @des start of process
	 */
   public function indexAction() 
   {
    // @global controller set up
  	$this->setEnv();
  		
  	// load session expired
  	if($this->accountModel->isExpiredCheck($this->helperAdapter->session->udata[0]['usrid']) === true){
  		$this->helperAdapter->Logout($this->redirect()->toUrl('/server/register/'));
  	}
  	
  	// make sure user is logged in
  	if($this->helperAdapter->isLogged() !== true){
  		$this->helperAdapter->Logout($this->redirect()->toUrl('/server/register/'));
  	}
	
  	// request process if available
    if($this->getEvent()->getRouteMatch()->getParam('do') == 'logout'){
      $this->accountModel->logout($this->helperAdapter->session->udata[0]['usrid'], $this->redirect()->toUrl('/server/register'));
    }
      
    // remove user
   	if($this->getEvent()->getRouteMatch()->getParam('remove') > 0){
      $this->accountModel->resignUser($this->helperAdapter->session->udata[0]['acoid'], $this->getEvent()->getRouteMatch()->getParam('remove'));
    }
      
    // make master
  	if($this->getEvent()->getRouteMatch()->getParam('master') > 0){
      $this->accountModel->userToMaster($this->helperAdapter->session->udata[0]['acoid'], $this->getEvent()->getRouteMatch()->getParam('master'));
    }
    
  	// make master
  	if($this->getEvent()->getRouteMatch()->getParam('player') > 0){
      	$this->accountModel->userToPlayer($this->helperAdapter->session->udata[0]['acoid'], $this->getEvent()->getRouteMatch()->getParam('player'));
    }

		
		$this->mergeErrors();
		$this->defineTplVars();

		$onlineUsers = $this->accountModel->areAccountPlayerOnline($this->helperAdapter->session->udata[0]['acoid']);
		$this->tplVars['onlineUsers'] = $onlineUsers;
		
		$view = new ViewModel($this->tplVars);
		
		$errMsg = new ViewModel(array('errmsg' => $this->tplVars['error']));
		$errMsg->setTemplate('server/global/errmsg');
		$this->tplVars['error'] = array();
		$view->addChild($errMsg, 'errmsg');
		
		// success handling
  	if($this->helperAdapter->session->success != ''){
  		
  		$sysMsg = new ViewModel(array('sysmsg' => $this->helperAdapter->session->success));
      	$sysMsg->setTemplate('server/global/sysmsg');
      	$this->helperAdapter->session->success = '';
      	$view->addChild($sysMsg, 'sysmsg');
  		
  	}
    	
    if($this->helperAdapter->session->account && $this->helperAdapter->session->udata[0]['asipermissions'] == 69) {
      $this->tplVars['account'] = $this->helperAdapter->session->account;
    	$invitetpl = new ViewModel($this->tplVars);
			$invitetpl->setTemplate('server/account/invite');
			$view->addChild($invitetpl, 'account');
    }

    $navitpl = new ViewModel();
		$navitpl->setTemplate('server/global/navi');
		$view->addChild($navitpl, 'naviConf');
		
		// user is not assigned to any account
		if($this->helperAdapter->session->udata[0]['noaco'] == 1) {
			$opttpl = new ViewModel(array('lingo' => $this->Lingo->String)); 
			$opttpl->setTemplate('server/account/option');
			$this->tplVars['option'] = array();
			$view->addChild($opttpl, 'option');
			
		}else{
			
      // user is asigned to account
			$users = $this->accountModel->getAccountUsers($this->helperAdapter->session->udata[0]['acoid']);
	    $usertpl = new ViewModel(array('users' => $users, 'data' => $this->tplVars['onlineUsers'], 'lingo' => $this->Lingo->String, 'helper' => $this->helperAdapter));
			$usertpl->setTemplate('server/account/users');
			$this->tplVars['users'] = array();
			$view->addChild($usertpl, 'users');
		
		}

      return $view;
  } 


	/**
	 * @desc User has no account as need to choose how he wants to go ahead
	 */
    public function optionAction()
    {
    	// @global controller set up
  		$this->setEnv();
  		
  		$this->mergeErrors();
  		$this->defineTplVars();
  		 
  		$view = new ViewModel($this->tplVars);
  		
  		$errMsg = new ViewModel(array('errMsg' => $this->tplVars['error']));
  		$errMsg->setTemplate('server/global/errmsg');
  		$this->tplVars['error'] = array();
  		$view->addChild($errMsg, 'errmsg');
  		
  		$navitpl = new ViewModel();
  		$navitpl->setTemplate('server/global/navi');
  		$view->addChild($navitpl, 'naviConf');
  		
      return $view;
    	
    }


    /**
     * @desc define template vars to start
     */
    private function defineTplVars() 
    {
    	$this->tplVars['error']		= $this->error;
    	$this->tplVars['data']		= '';
    	$this->tplVars['lingo'] 	= $this->Lingo->String;
    	 
    	return false;
    }
    
    
    /**
     * @desc set up dependencies
     */
    private function setEnv()
    {
    	// get helper
    	$this->getHelper();
    	// get model
    	$this->getModel();
    	// get language
    	$this->getLingo('de_DE');
    	// set lingo in model
    	$this->accountModel->setLingo($this->Lingo);
    	
    	$this->layout = $this->layout();
    	$this->layout->lang = $this->Lingo->String;
    }
    

    /**
     * @desc merge the errors for printing
     */
    private function mergeErrors()
    {
    	$this->error = array_merge($this->error, $this->accountModel->getError());
    	$this->tplVars['error'] = $this->error;
    	return false;
    }

    
    public function getModel() 
    {
      if (!$this->accountModel) {
          $sm = $this->getServiceLocator();
          $this->accountModel = $sm->get('account-model');
      }
     return false;
    } 
    
    
    public function getLingo($lang) 
    {
    	if (!$this->Lingo) {
    		$this->Lingo = new Lingo($lang);
    	}
    	return false;
    }
    
    
    public function getHelper() 
    {
      if (!$this->helperAdapter) {
          $sm = $this->getServiceLocator();
          $this->helperAdapter = $sm->get('helper-adapter');
      }
      return false;
    } 
    
}
