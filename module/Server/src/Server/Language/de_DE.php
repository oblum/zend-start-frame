<?php

namespace Server\Language;

class de_DE {
	
	public $String;
	
	public function __construct(){
		
		$this->String =  array(
		
			'welcome'			=> 'Willkommen',
			'register'			=> 'Anmelden',
			'join'				=> 'Registrieren',
			'login'				=> 'Login',
			'gameName'			=> 'Piratenkampf Server 2',
			'userName'			=> 'Benutzername',
			'email'				=> 'eMail',
			'emailConfirm'		=> 'Best&auml;tige eMail',
			'code'				=> 'Code',
			'password'			=> 'Passwort',
			'showHome'			=> 'Home',
			'showStarted'		=> 'Spiel Beschreibung',
			'showLogin'			=> 'Anmelden',
			'showRegister'		=> 'Registrieren',
			'showForum'			=> 'Forum',
			'showHistory'		=> 'Spiel Historie',
			'forgotsubj'		=> 'Piratenkampf - Neues Password',
			'goodby'			=> 'Bis bald <br /><br />Das Piratenkampf Team.',
			'helpCode'			=> 'Du brauchst den Code nur wenn du einem existierendem Account beitreten m&ouml;chtest. Solltest du keinen Code haben belasse das Feld einfach wie es ist.',
			'activtrue'			=> 'Dein Konto ist jetzt aktiviert.',
			'activefalse'		=> 'Der aktivierungscode ist ung&uuml;ltig oder wurde schon benutzt.',
			'loginfail'			=> 'Der Login schlug fehl.',
			'inuse'				=> 'Der Benutzername oder die email Adresse ist schon vergeben.',
			'accountcreated'	=> 'Dein Konto wurde erstellt.',
			'registerfailed'	=> 'Die Registrierung ist fehlgeschlagen.',
			'entername'			=> 'Gebe einen Benutzernamen an.',
			'invalusername'		=> 'Der Benutzername ist nicht erlaubt, erlaubt sind Buchstaben und Zahlen sowie "-", desweiteren muss der Benutzername zwischen 3 und 15 Zeichen haben.',
			'emailconfail'		=> 'Die eMails sind nicht gleich.',
			'invalemail'		=> 'Das ist keine korrekte eMail Adresse.',
			'invalcode'			=> 'Der eingegebene Code ist falsch.',
			'enterpass'			=> 'Gebe ein Passwort an',
			'accountnousers'	=> 'Der Account hat keinen User zugewiesen, wie ich immer es dazu gekommen ist. Email an blumanski@gmail.com',
			'naviConf'			=> 'Einstellungen',
			'naviUsers'			=> 'Benutzer',
			'naviEmail'			=> 'eMail',
			'naviExit'			=> 'Ausloggen',
			'error659'			=> 'Der angegebene Code existiert existiert nicht.',
			'invitingCode'		=> 'Der neue Code ist',
			'invitingButton'	=> 'Mitspieler einladen',
			'assignAlready'		=> 'Der Spieler spielt schon in einem anderem Account.',
			'accoMaster'		=> 'Master',
			'accoPlay'			=> 'Spieler',
			'delete'			=> 'L&ouml;schen',
			'toAdmin'			=> 'Admin status',
			'toPlayer'			=> 'Spieler status',
			'optionCode'		=> 'Trete Account bei',
			'optionNew'			=> 'Starte neuen Account',	
			'noAccount'			=> 'Du bist im Moment keinem Account zugewiesen.',
			'noAccountSub'		=> 'W&auml;hle eine Option aus, du kannst einen neuen Account gr&uuml;nden oder einen Code benuzten um einem Account beizutreten.',	
			'notInThisAccount'	=> 'Ein Spieler mit dieser Id is nicht im Account.',
			'error85'			=> 'Error: 85 - Ein Fehler ist aufgetreten.',
			'error86'			=> 'Error: 86 - Ein Fehler ist aufgetreten.',
			'error87'			=> 'Error: 87 - Ein Fehler ist aufgetreten.',
			'error88'			=> 'Error: 88 - Ein Fehler ist aufgetreten.',
			'error89'			=> 'Error: 89 - Ein Fehler ist aufgetreten.',
			'error90'			=> 'Error: 90 - Ein Fehler ist aufgetreten.',
			'admLogin'			=> 'Administrator Login',
			'admSearch'			=> 'Suchen',
			'admSearchUser'		=> 'Spieler suchen',
			'admSearchAccount'	=> 'Account suchen',
			'admObject'			=> 'suchen',
			'defObject'			=> 'Insel',
			'forgotPassword'	=> 'Password vergessen',
			'requestPassword'	=> 'Password anfordern',
			'forgottenMsg'		=> 'Hallo, siehe untestehend dein neues Password f&uuml;r deinen Piratenkampf Account.',
			'successForgot'		=> 'Bitte pr&uuml;fe dein eMail Konto, eine eMail mit dem neuem Passwort wurde dir zugesendet.',
			'admUsers'			=> 'Spieler',
			'admAccounts'		=> 'Accounts',
			'admServerConf'		=> 'Server-Konfiguration',
				
			
			'gettingHead'			=> 'Kurz Erkl&auml;rung',
			'gettingStartedShort'	=> '<p>Ein paar Eckdaten um das Spiel zu erkl&auml;ren.
				Ein Server hat z.B. 36 Ozeane welche in einem Quadrat angeordent sind.</p>
					
<pre>

01|02|03|04|05|06|
07|08|09|10|11|12|
13|14|15|16|17|18|
19|20|21|22|23|24|
25|26|27|28|29|30|
31|32|33|34|35|36|

</pre>
				<p>
				Jeder Ozean hat 100 Inselgruppen und jede Inselgruppe beherbegt bis zu 4 Inseln. Das ist die Piratenkampf Welt, es geht nat&uuml;rlich darum die Macht &uuml;ber die Welt zu erlangen.
				<br /><br /><a href="/beschreibung" title="Weiter lesen">Weiter lesen</a></p>',
				
				
				
			'gettingStarted'	=> '<h3>Spiel Beschreibung</h3><p>Ein paar Eckdaten um das Spiel zu erkl&auml;ren.
			Ein Server hat z.B. 36 Ozeane welche in einem Quadrat angeordent sind.</p>
			
<pre>

01|02|03|04|05|06|
07|08|09|10|11|12|
13|14|15|16|17|18|
19|20|21|22|23|24|
25|26|27|28|29|30|
31|32|33|34|35|36|

</pre>
			<p>	
			Jeder Ozean hat 100 Inselgruppen und jede Inselgruppe beherbegt bis zu 4 Inseln. Das ist die Piratenkampf Welt, es geht nat&uuml;rlich darum die Macht &uuml;ber die Welt zu erlangen.
			</p>
			<p>
			Jeder Ozean hat bis zu 50 Spieler welche jeweils mit einer Insel starten. Im Verlauf des Spiels wirst du neue Inseln kolonisieren und erobern.
			</p>
			<p>
			In der Regel bilden diese 50 Spieler eine oder mehrere Allianzen im jeweiligem Ozean denn alleine kann man die Macht erstmal nicht an sich reissen.
			</p>
			<p>
			Eine Allianz besteht in der Regel aus 20 Spielern welche sich im selben Ozean befinden, es werden oft sogenannte Wings gegr&uuml;ndet um mehr Spieler aufnehmen zu k&ouml;nnen. Das w&auml;hren z.B. zwei Allianzen im selben Ozean welche als eine Einheit gelten und unter dem gleichem Namen segeln.
			</p>
			<p>
			Das geschieht eventuell in allen Ozeanen, es formieren sich Allianzen welche politische B&uuml;ndnisse untereinander schliessen k&ouml;nnen.
			</p>
			<p>
			Eine Allianz alleine kann eine Welt nicht so einfach erobern, es geh&ouml;ren Strategie und diplomatisches Verhandlungschick dazu um eine Welt zu gewinnen. Man muss die richtigen Partner Allianzen haben und als wichtigstes aktive Spieler in der Allianz. Inaktive Spieler sind leider wertloser Balast.
			</p>
			<p>
			Die n&auml;chste Welt startet am 16.09.2012.</p>',
				
				
				
			'txtWelcome'		=> '<h3>Piratenkampf pr&auml;sentiert</h3>
			<h4>
			Server 1 - Classic Style
			</h4>
			<p>	
				<strong>
					Start: Sonntag, 16. September 2012, 14:00 Uhr.
				</strong>
			</p>
			<p>
			Fuuml;r alle Spieler die sich bis 23. September 2012, 20:00 Uhr anmelden Premium bis 30. September 2012 kostenlos.
			</p>
				
<pre>

01|02|03|04|05|06|
07|08|09|10|11|12|
13|14|15|16|17|18|
19|20|21|22|23|24|
25|26|27|28|29|30|
31|32|33|34|35|36|

</pre>
			<p>
			Ozeane: 36 (6x6) (nicht alle Ozeane k&ouml;nnen zu Beginn besiedelt werden!), 50 Spieler pro Ozean<br />
			Startinsel: 1 Insel, vorausgebaut<br />
			Ozeanwahl: Nein<br />
			Wachturm: Ja<br />
			Katapulte: Ja<br />
			Noobschutz: Nein<br />
			Member pro Allianz: 20 inkl. Administratoren<br />
			Premium: Ja<br />
			Lows: zum Start 75 Lows pro Ozean, es besteht die Option auf weitere Lows<br />
			Urlaubsfunktion: Ja<br />
			Support: Ja<br />
			Aktive Verfolgung von Multis: Ja<br />
			Kolotool: Oceanview by KayG<br />
			Englische Sprachversion: Ja<br />
			Ingame Werbefrei: vorerst Ja<br />
			</p>
			<p>	
			Informationen zum Premium Account
			</p>
			<p>
			Normale Laufzeiten (nur auf einem Server nutzbar)<br />
			6 Monate 8&euro;<br />
			12 Monate 14&euro;<br />
			</p>
			<p>
			Spezial Laufzeiten ($uuml;bertragbar auf weitere Server)<br />
			18 Monate 20&euro;<br />
			24 Monate 24&euro;<br />
			ein Leben lang XXL 99&euro;<br />
			</p>
			<p>
			Was kann mein Premium Account?
			</p>
			<ul>
				<li>Membernamen &auml;ndern</li>
				<li>Member- und Allianzbild einf&uuml;gen</li>
				<li>Nachrichten an alle Allianzmember gleichzeitig schreiben</li>
				<li>Einfacherer Wechsel zwischen den Inseln (vor und zur&uuml;ck)</li>
				<li>30 Tage Urlaubsvertretung</li>
				<li>Komplett werbefrei</li>
				<li>Eigenen Account l&ouml;schen</li>
				<li>Baubot f&uuml;r Einheiten (Bau jeweils 1 mal t&auml;glich)</li>
				<li>Baubot f&uuml;r Geb&auml;ude (Bau jeweils alle 30 Minuten)</li>
			</ul>
				',
				
			'txtHistory'		=> 'Die beste Allianz die es jemals gab das war dieKippe auf Inselkampf Server 7.',
				
			'navScreens'		=> 'Screenshots',
			'txtScreens'		=> 'All screenshots in here',
			'navFAQ'			=> 'FAQ',
			'txtFaq'			=> 'All FAQ in here',
			'navContact'		=> 'Kontakt',
			'txtContact'		=> 'Kontakt is in here',
			
		);
		
	}

}